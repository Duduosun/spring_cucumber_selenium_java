package core;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.Augmenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NeoReportingFactory
{
    private final String pageName;
    private final String currentTestName;
    private final WebDriver currentWebDriver;
    private final String runningAgainstSite;

    private static final Logger LOG = LoggerFactory.getLogger(NeoReportingFactory.class);

    private String screenshotsDirectory;
    private String htmlFilesDirectory;

    private static String fileSeparator = File.separator;

    //private int incrementalScreenShotCounter;

    public NeoReportingFactory(final String pageName, final String currentTestName, final WebDriver currentWebDriver, final String runningAgainstSite, final String newProjectDir)
    {
        this.pageName = pageName;
        this.currentTestName = currentTestName;
        this.currentWebDriver = currentWebDriver;
        this.runningAgainstSite = runningAgainstSite;
        //this.incrementalScreenShotCounter = 0;

        setProjectDir(newProjectDir);
    }

    public void takeScreenShot()
    {
        takeScreenShotAction("","");
    }

    public void takeScreenShot(final String timestamp, final String suffix)
    {
        takeScreenShotAction(timestamp, suffix);
    }

    public void takeScreenShotAction(final String timestamp, final String suffix)
    {
        final WebDriver augmentedDriver = new Augmenter().augment(getCurrentWebDriver());
        final File scrFile = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);

        final String newTimestamp = (timestamp.equals("") ? new SimpleDateFormat("yyyyMMddhhmm").format(new Date()) : timestamp);
        //final String newFileName = new StringBuilder(getRunningAgainstSite().toUpperCase()).append(getCurrentTestName()).append("-").append(getPageName()).toString();
        final String newFileName = getRunningAgainstSite().toUpperCase() + "-" + getCurrentTestName() + "-" + getPageName();
        final String newSuffix =  (suffix.equals("") ? "" : "_" + suffix);

        //LOG.info("Here: " + projectDir);
        //LOG.info("Here 2: " + screenshotsDirectory);

        try
        {
            FileUtils.copyFile(scrFile, new File(screenshotsDirectory + newTimestamp + "_" + newFileName + newSuffix + ".png"));
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }
    }

    /*public void takeIncrementalScreenShot()
    {
        final String incrementalCounter = String.valueOf(++incrementalScreenShotCounter);
        final WebDriver augmentedDriver = new Augmenter().augment(getCurrentWebDriver());
        final File scrFile = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
        final String fileName = new StringBuilder(incrementalCounter).append("-").append(getCurrentTestName()).append("-").append(getPageName()).toString();

        try
        {
            FileUtils.copyFile(scrFile, new File("./target/incremental-screenshots/" + fileName + ".png"));
        }
        catch (final IOException e)
        {
            e.printStackTrace();
        }
    }*/

    public void writeHTMLOfCurrentPageToFile()
    {
        //final String fileName = new StringBuilder(getRunningAgainstSite().toUpperCase()).append(getCurrentTestName()).append("-").append(getPageName()).toString();
        final String fileName = getRunningAgainstSite().toUpperCase() + "-" + getCurrentTestName() + "-" + getPageName();

        try
        {
            final File htmlFile = new File(htmlFilesDirectory + fileName + ".html");

            if (htmlFile.getParentFile().mkdir() && htmlFile.createNewFile())
            {
                LOG.info("Successfully made directory [{}] with file [{}]", htmlFile.getParentFile().getName(), htmlFile.getName());
            }

            final Writer writer = new BufferedWriter(new FileWriter(htmlFile));
            final WebElement htmlElement = getCurrentWebDriver().findElement(By.tagName("html"));
            final String contents = (String)((JavascriptExecutor)getCurrentWebDriver()).executeScript("return arguments[0].innerHTML;", htmlElement);
            writer.write(contents);
        }
        catch (final IOException ex)
        {
            ex.printStackTrace();
        }
    }

    public String getPageName()
    {
        return pageName;
    }

    public String getCurrentTestName()
    {
        return currentTestName;
    }

    public String getRunningAgainstSite()
    {
        return runningAgainstSite;
    }

    protected WebDriver getCurrentWebDriver()
    {
        return currentWebDriver;
    }

    protected void setProjectDir(String newProjectDir)
    {
        String reportDirectory = newProjectDir + fileSeparator + "reports" + fileSeparator;

        screenshotsDirectory = reportDirectory + "screenshots" + fileSeparator;
        htmlFilesDirectory = reportDirectory + "html-files" + fileSeparator;
    }
}