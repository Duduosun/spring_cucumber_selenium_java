package core;

import com.google.common.collect.Lists;

import java.util.ArrayList;

public class NeoCheckpoint
{
    private core.PageWrapper page;

    private String description;
    private String passMessage;
    private String failMessage;
    private static final String descriptionHeader = "### CHECKPOINT ###";
    private static final String passMessageHeader = "### PASS : ";
    private static final String failMessageHeader = "### FAIL : ";
    private static final String messageLine = "---------------------------------------------------------------------";
    private boolean descriptionPrinted = false;

    public NeoCheckpoint(core.PageWrapper newPage)
    {
        this.page = newPage;
    }

    public void execute(boolean conditionResult)
    {
        if(!descriptionPrinted)
        {
            printDescription();
        }

        if (conditionResult)
        {
            core.PageWrapper.LOG.info(passMessage);
            core.PageWrapper.LOG.info(messageLine);
        }
        else
        {
            PageWrapper.LOG.warn(failMessage);
            PageWrapper.LOG.info(messageLine);

            page.getReportingFactory().takeScreenShot();
            page.getReportingFactory().writeHTMLOfCurrentPageToFile();

            fail(failMessage);
        }
    }

    static public void fail(String message)
    {
        throw new AssertionError(message == null ? "" : message);
    }

    public void printDescription()
    {
        PageWrapper.LOG.info("");
        PageWrapper.LOG.info(messageLine);
        PageWrapper.LOG.info(descriptionHeader);
        PageWrapper.LOG.info(description);

        descriptionPrinted = true;
    }

    public void setDescription(String s, Object... objects)
    {
        description = String.format(s, objects);

        if(!descriptionPrinted)
        {
            printDescription();
        }
    }

    public void setFailMessage(String s, Object... objects)
    {
        ArrayList<Object> objs = Lists.newArrayList(objects);
        objs.add(page.getReportingFactory().getPageName());
        objs.add(page.getReportingFactory().getCurrentTestName());

        failMessage = String.format(failMessageHeader + s +
                "\nTaking screen shot of [%s] for current test [%s]", objs.toArray());
    }

    public void setPassMessage(String s, Object... objects)
    {
        passMessage = String.format(passMessageHeader + s, objects);
    }
}