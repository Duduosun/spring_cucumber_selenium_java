package core;

import java.util.ArrayList;
import java.util.Collections;

public class NeoProduct
{
    private String id;
    private String name;
    private String summary;

    private String currentCode;
    private String currentColour;
    private String currentSize;

    private ArrayList<String> codes;            // Array of available codes for product
    private ArrayList<String> colours;          // Array of available colours for product
    private ArrayList<String> sizesInStock;     // Array of available sizes In Stock for product
    private ArrayList<String> sizesOutOfStock;  // Array of available sizes Out of Stock for product

    public NeoProduct(final String newId, final String newName, final String newSummary)
    {
        this.id = newId;
        this.name = newName;
        this.summary = newSummary;

        this.codes = new ArrayList<String>();
        this.colours = new ArrayList<String>();
        this.sizesInStock = new ArrayList<String>();
        this.sizesOutOfStock = new ArrayList<String>();
    }

    /*
     * SET methods
     */

    public String setId(final String newId)
    {
        this.id = newId;

        return this.id;
    }

    public String setName(final String newName)
    {
        this.name = newName;

        return this.name;
    }

    public String setSummary(final String newSummary)
    {
        this.summary = newSummary;

        return this.summary;
    }

    public void setCodes(final String... newCodes)
    {
        this.codes.clear();

        Collections.addAll(this.codes, newCodes);
    }

    public void setColours(final String... newColours)
    {
        this.colours.clear();

        Collections.addAll(this.colours, newColours);
    }

    public void setSizesInStock(String... sizes)
    {
        this.sizesInStock.clear();

        Collections.addAll(this.sizesInStock, sizes);
    }

    public void setSizesOutOfStock(String... sizes)
    {
        this.sizesOutOfStock.clear();

        Collections.addAll(this.sizesOutOfStock, sizes);
    }


    /*
     * GET methods
     */

    public String getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getSummary()
    {
        return this.summary;
    }

    public String getCode(final int index)
    {
        return this.codes.get(index);
    }

    public String getDefaultCode()
    {
        return this.codes.get(0);
    }

    public String getColour(final int index)
    {
        return this.colours.get(index);
    }

    public String getDefaultColour()
    {
        return this.colours.get(0);
    }

    public String getSizeInStock(final int index) {
        return this.sizesInStock.get(index);
    }

    public String getDefaultSizeInStock() {
        return this.sizesInStock.get(0);
    }

    public String getSizeOutOfStock(final int index) {
        return this.sizesOutOfStock.get(index);
    }

    public String getDefaultSizeOutOfStock() {
        return this.sizesOutOfStock.get(0);
    }
}
