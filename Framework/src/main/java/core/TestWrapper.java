package core;

import com.google.common.base.Stopwatch;
import io.selendroid.common.SelendroidCapabilities;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

@Component
public class TestWrapper
{
    private static final Logger LOG = LoggerFactory.getLogger(TestWrapper.class);
    private static Stopwatch testTimer = Stopwatch.createUnstarted();

    // List of Web Drivers
    //final List<WebDriver> drivers = new ArrayList<>();
    public WebDriver singleDriver;

    public URL seleniumHubURL = null;

    public String hostURL = null;
    public String siteURL = null;
    public String backendURL = null;

    public String csBackendSiteURL = null;
    public String hacBackendSiteURL = null;
    public String hmcBackendSiteURL = null;
    public String cmsCockpitBackendSiteURL = null;
    public String productCockpitBackendSiteURL = null;

    public static String testCountry = null;
    public String testOs = null;
    public String testBrowserName = null;
    public String testBrowserVersion = null;

    public String siteLocalizedURL = null;

    public String projectDir = null;

    // Variables to keep information about Test details
    public String testMethodName = null;
    public String testClassName = null;
    public String driverName = null;
    public String browserSize = null;
    public Boolean mobileFlag = false;

    public ChromeOptions chromeoptions;
    public WebDriverWait wait;




    @Rule
    public TestName testName = new TestName();

    public void writeBeginningTestToOut()
    {
        LOG.info("*******************************************************************************************************");
        LOG.info("*******************************************************************************************************");
        LOG.info("***  TEST NAME:           [{}]", testClassName);
        LOG.info("***  TEST CASE NAME:      [{}]", testMethodName);
        LOG.info("***  WEBSITE URL:         [{}]", siteURL);
        LOG.info("***  RUNNING AGAINST:     [{}]", testCountry.toUpperCase());
        LOG.info("***  WITH WEB DRIVER:     [{}]", driverName);
        LOG.info("***  BROWSER SIZE:        [{}]", browserSize);
        LOG.info("*******************************************************************************************************");
        LOG.info("*******************************************************************************************************");
    }

    public void writeEndTestToOut()
    {
        String testTimerString = testTimer.elapsed(TimeUnit.HOURS) + " h, "
                + (testTimer.elapsed(TimeUnit.MINUTES) % 60) + " min, "
                + (testTimer.elapsed(TimeUnit.SECONDS) % 60) + " sec";

        LOG.info("");
        LOG.info("*******************************************************************************************************");
        LOG.info("***  TEST CASE EXECUTION REPORT");
        LOG.info("***  ");
        LOG.info("***  TEST NAME:         [{}]", testClassName);
        LOG.info("***  TEST CASE NAME:    [{}]", testMethodName);
        LOG.info("***  EXECUTION TIME:    [{}]", testTimerString);
        LOG.info("*******************************************************************************************************");
        LOG.info("");
    }

    // Method used for debug on Test level
    public void logText(final String text, final Object... data)
    {
        LOG.info(text, data);
    }

    /*public void logDriver(final WebDriver driver)
    {
        LOG.info("");
        LOG.info("*******************************************************************************************************");
        LOG.info("*** DRIVER:   [{}]", driver.toString());
        LOG.info("*******************************************************************************************************");
        LOG.info("");
    }*/

    @Before
    @PostConstruct
    public void setUp()
    {
        testTimer.reset().start();

        setUpTestInformation();

        setUpListOfWebDrivers();
        setUpSiteToRunAgainst();
        setUpDriverInformation();

        writeBeginningTestToOut();
    }

    protected void setUpSiteToRunAgainst()
    {
        hostURL = System.getProperty("hostURL");        //  e.g. http://www.tb303.local:9001
        siteURL = System.getProperty("siteURL");        //  e.g. http://www.tb303.local:9001/hegstorefront
        backendURL = System.getProperty("backendURL");  //  e.g. https://backoffice.tb303.local:9002

        // Hybris Backend Services
        csBackendSiteURL = backendURL + "/customerservice";
        hacBackendSiteURL = backendURL + "/hac";
        hmcBackendSiteURL = backendURL + "/hmc/hybris";
        cmsCockpitBackendSiteURL = backendURL + "/cmscockpit";
        productCockpitBackendSiteURL = backendURL + "/productcockpit";

        // Selenium Test Variables
        testCountry = System.getProperty("testCountry");
        testOs = System.getProperty("testOs");
        testBrowserName = System.getProperty("testBrowserName");
        testBrowserVersion = System.getProperty("testBrowserVersion");

        siteLocalizedURL = siteURL;      //+ "/";

        projectDir = System.getProperty("projectDir");      // Directory where pom.xml lives
    }

    protected void setUpListOfWebDrivers()
    {
        DesiredCapabilities cap = new DesiredCapabilities();
        SelendroidCapabilities selCap = new SelendroidCapabilities();


        try
        {
            // Selenium Hub
            seleniumHubURL =  new URL(System.getProperty("seleniumHubURL"));

            // --------------------------------------------------------------------
            // BROWSER
            //
            // According to the http://www.w3schools.com/browsers/browsers_stats.asp from 2015.05.08
            // Chrome = 63.9% (41, 42)
            // Firefox = 21.6% (36, 37)
            // IE = 8% (8, 9, 10, 11)
            // Safari = 3.8% (8)

            switch(System.getProperty("testBrowserName"))
            {


                // ### Chrome Driver
                default:
                case "chrome":
                    cap = DesiredCapabilities.chrome();
                    break;


                // ### Firefox Driver
                case "firefox":
                    cap = DesiredCapabilities.firefox();
                    cap.setCapability(FirefoxDriver.PROFILE, new FirefoxProfile());
                    break;

                // ### Internet Exlorer
                case "ie":
                    cap = DesiredCapabilities.internetExplorer();
                    cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                    break;

                // ### Safari Driver on Mac
                case "safari":
                    cap = DesiredCapabilities.safari();
                    System.setProperty("webdriver.safari.noinstall", "true");
                    //cap.setCapability("platformName", "iOS");
                    //cap.setCapability("deviceName", "iPhone");
                    break;

                // ### Chrome Driver
                case "androidChrome":
                    File appDir = new File("src/test/resources/ContactManager.apk");
                    cap = DesiredCapabilities.android();
                    cap.setCapability("platformName","Android");
                    cap.setCapability("platformVersion","4.4");
                    cap.setCapability("deviceName","Nexus 5");
                    cap.setCapability("app",appDir.getAbsolutePath());
                    cap.setCapability("browserName","chrome");
                    break;

                // ### Safari Driver on Mac
                case "iphoneSafari":
                    cap = new DesiredCapabilities();
                    cap.setCapability("deviceName" ,"iPhone");
                    cap.setCapability("platformName" , "ios");
                    cap.setCapability("udid", "9d577285e906340e08734b70439b3f5a0b63797f");
                    cap.setCapability("platformVersion","8.4");
                    cap.setCapability("browserName", "safari");

                    break;

                case "ipadSafari":
                    cap = new DesiredCapabilities();
                    cap.setCapability("deviceName" ,"iPad");
                    cap.setCapability("platformName" , "ios");
                    cap.setCapability("udid", "77fc75b7f8e7912e6d7f292edf4ddee6698b2f79");
                    cap.setCapability("platformVersion","8.1.2");
                    cap.setCapability("browserName", "safari");

                    break;

            }

            // --------------------------------------------------------------------
            // PLATFORM
            //
            switch(System.getProperty("testOs"))
            {
                default:
                case "windows":
                    cap.setPlatform(Platform.WINDOWS);
                    break;
                case "linux":
                    cap.setPlatform(Platform.LINUX);
                    break;
                case "mac":
                    cap.setPlatform(Platform.MAC);
                    break;
                case "android":
                    cap.setPlatform(Platform.ANDROID);
                    mobileFlag = true;
                    break;
                case "ios":
                    cap.setPlatform(Platform.ANY);
                    mobileFlag = true;
                    break;
            }

            // --------------------------------------------------------------------
            // BROWSER CAPABILITIES
            //
          //  cap.setCapability("elementScrollBehavior", true);
           // cap.setCapability("applicationCacheEnabled", false);
           // cap.setCapability("javascriptEnabled", true);
           // cap.setCapability("nativeEvents", false);

            try
            {
                // Create new Remote Web Driver
                if (System.getProperty("testOs").equals("android"))
                {
                    singleDriver = new RemoteWebDriver(seleniumHubURL, cap);
                } else
                {
                    singleDriver = new RemoteWebDriver(seleniumHubURL, cap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        catch (final MalformedURLException e)
        {
            e.printStackTrace();
        }

        // Specify Global Timeouts
        singleDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        if(!System.getProperty("testBrowserName").equals("iphone"))
        {
            singleDriver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
        }

        singleDriver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);

        // Specify Browser position and size
        //singleDriver.manage().window().setPosition(new Point(0, 0));
        //singleDriver.manage().window().setSize(new Dimension(1366,768));

        // According to the http://www.w3schools.com/browsers/browsers_display.asp from 2014.06.19
        // 1366x768 = 31%
        // 1920x1080 = 13%
        // 1280x1024 = 8%
        // 1280x800 = 7%
        // 1024x768 = 6%
    }

    // Get information about test
    protected void setUpTestInformation()
    {
        testClassName = super.getClass().getSimpleName();
        testMethodName = testName.getMethodName();
    }

    protected void setUpDriverInformation()
    {
        driverName = singleDriver.toString();
        //browserSize = singleDriver.manage().window().getSize().toString();
    }






    @After
    @PreDestroy
    public void cleanUp()
    {

        //singleDriver.close();   // Extra safety
        singleDriver.quit();

        if (testTimer.isRunning())
        {
            testTimer.stop();
        }

        writeEndTestToOut();
    }
}
