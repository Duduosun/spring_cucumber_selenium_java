package core;

/**
 * NeoWebElement
 *
 * newElementType:
 *      default
 *
 * newLookupMethod:
 *      neo
 *      xpath
 *      id
 */
public class NeoWebElement
{
    private String elementType;
    private String elementXPath;

    public NeoWebElement(final String newElementType, final String newLookupMethod, final String... newLookupString)
    {
        this.elementType = newElementType;

        if(newLookupMethod.equals("neo"))
        {
            setXPath(PageWrapper.neoXPathBuilder(newLookupString));
        }

        if(newLookupMethod.equals("xpath"))
        {
            setXPath(newLookupString[0]);
        }

        if(newLookupMethod.equals("id"))
        {
            setXPath(PageWrapper.neoXPathBuilder(";id::" + newLookupString[0]));
        }
    }

    public String getType()
    {
        return this.elementType;
    }

    public String getXPath()
    {
        return this.elementXPath;
    }

    public void setXPath(final String newXPath)
    {
        this.elementXPath = newXPath;
    }
}