package core;

import com.google.common.base.Function;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class PageWrapper
{

   private TestWrapper testWrapper;

    private WebDriver currentWebDriver;
    private NeoReportingFactory reportingFactory;

    // Timeouts
    public static final int PAGE_LOAD_TIMEOUT = 60;
    public static final int PAGE_LOAD_TIMEOUT_LONG = 120;
    public static final int FLUENT_WAIT_POOLING_TIME = 2;
    public static final int FLUENT_WAIT_TIMEOUT = 30;

    public static final int NEO_WAIT_SHORT_TIMEOUT = 10;
    public static final int NEO_WAIT_MEDIUM_TIMEOUT = 30;
    public static final int NEO_WAIT_NORMAL_TIMEOUT = 60;
    //public static final int NEO_WAIT_LONG_TIMEOUT = 120;

    public static final String NEO_DATA_ID = "data-tid";
    public static final String NEO_STEP_PREFIX = "# STEP : ";

    private static Stopwatch pageLoadTimer = Stopwatch.createUnstarted();
    private static Stopwatch findElementTimer = Stopwatch.createUnstarted();

    protected WebDriverWait wait;

    public static final Logger LOG = LoggerFactory.getLogger(PageWrapper.class);

    // Page Constructor
    public PageWrapper(WebDriver driver, TestWrapper neoTestWrapper)
    {
        setCurrentWebDriver(driver);
        setNeoTestWrapper(neoTestWrapper);

        reportingFactory = new NeoReportingFactory(PageWrapper.class.getSimpleName(), neoTestWrapper.testClassName, driver, neoTestWrapper.testCountry, neoTestWrapper.projectDir);

        waitForPageReady(driver);
    }


    /***************************************************************
     *  CLASS GET & SET
     */

    public TestWrapper getNeoTestWrapper() {
        return testWrapper;
    }

    public void setNeoTestWrapper(TestWrapper neoTestWrapper) {
        this.testWrapper = testWrapper;
    }

    protected WebDriver getCurrentWebDriver()
    {
        return currentWebDriver;
    }

    public void setCurrentWebDriver(final WebDriver currentWebDriver)
    {
        this.currentWebDriver = currentWebDriver;
    }

    protected NeoReportingFactory getReportingFactory()
    {
        return reportingFactory;
    }

    /***************************************************************
     *   BUILDER METHOD
     *
     *   dataID =
     *       "<tagName>;<tagAttribute>:<comparator>:<tagAttributeValue>"
     *       "<tagAxis>::<tagName>;" - E.g "ancestor:tr"
     *
     *   <comparator> allowed values are '-' and '=' (default = '')
     *
     *   dataID =
     *       "div:id::cart", "a:::item_remove"
     *
     *   Examples:
     *       <div id="country_select>
     *           - "div:id::country_select"
     *           - ":id::country_select"
     *       <button data-id="goToSite">
     *           - "button:data-id::goToSite"
     *           - ":data-id::goToSite"
     *           - ":::goToSite"
     *       <span>Some Text</span>
     *           - "span:text()::Some Text"
     *           - "span:text():-:Text"
     *
     *      neoXPathBuilder()
     */
    public static String neoXPathBuilder(final String... dataID)
    {
        String xpString = "";
        String tagName = "";
        String tagAttrName = "";
        Boolean equalFlag = true;
        String tagAttrValue = "";

        //LOG.info("----------------------------------------------------");
        //LOG.info("From: {}", Arrays.toString(dataID));

        for(String element : dataID)
        {
            // ""       //*
            // "item"   //*[@data-tid="item"]
            if(!element.contains(";"))
            {
                if (element.equals(""))
                {
                    xpString = xpString + "//*";
                }
                else
                {
                    xpString = xpString + "//*[@" + NEO_DATA_ID + "=\"" + element + "\"]";
                }
            }
            else
            {
                String[] el2tbl = element.split(";");
                //LOG.info("el2tbl.length = {}", el2tbl.length);

                for (int i = 0; i < el2tbl.length; i++)
                {
                    // TAG
                    if (i == 0)
                    {
                        // "div;"
                        if (!el2tbl[i].isEmpty())
                            xpString = xpString + "//" + el2tbl[i];
                    }
                    // ATTRIBUTE
                    else
                    {
                        // "*;item"     to      "*;::item"
                        if (!el2tbl[i].contains(":"))
                        {
                            el2tbl[i] = "::" + el2tbl[i];
                        }

                        String[] el2 = el2tbl[i].split(":");
                        //LOG.info("el2.length = {}", el2.length);

                        // ";id::"
                        if (el2.length < 3)
                        {
                            xpString = xpString + "[@" + el2[0] + "]";
                        }
                        else
                        {
                            // Comparator
                            equalFlag = el2[1].isEmpty();

                            // Tag Attribute Name
                            if (el2[0].isEmpty()) {
                                tagAttrName = "@" + NEO_DATA_ID;
                            } else {
                                // "*;text()::item"
                                if (el2[0].equals("text()")) {
                                    if (equalFlag) {
                                        tagAttrName = ".";
                                    } else {
                                        tagAttrName = "text()";
                                    }
                                } else {
                                    tagAttrName = "@" + el2[0];
                                }
                            }



                            // Tag Attribute Value
                            tagAttrValue = el2[2];

                            if (equalFlag) {
                                // Equals
                                xpString = xpString + "[" + tagAttrName + "=\"" + tagAttrValue + "\"]";
                            } else {
                                // Contains
                                xpString = xpString + "[contains(" + tagAttrName + ",\"" + tagAttrValue + "\")]";
                            }
                        }
                    }
                }
            }
        }

        // For Debug purposes only
        //LOG.info("Final XPath: {}", xpString);
        //LOG.info("----------------------------------------------------");

        return xpString;
    }

    /***************************************************************
     *  BROWSER
     */

    public PageWrapper browserBack()
    {
        this.logText(NEO_STEP_PREFIX + "Going back one page via the browser's back button");

        getCurrentWebDriver().navigate().back();

        waitForPageReady(getCurrentWebDriver());

        return new PageWrapper(getCurrentWebDriver(), getNeoTestWrapper());
    }

    public void browserRefresh()
    {
        this.logText("Refreshing page [{}]", getCurrentWebDriver().getCurrentUrl());

        getCurrentWebDriver().navigate().refresh();

        waitForPageReady(getCurrentWebDriver());
    }


    /***************************************************************
     *  CHECKBOX SELECT
     */

    public void checkboxSelectByXPath(final boolean checkStatus, final String xpathString)
    {
        final WebElement webElement = findSingleElementByXPath("visible", xpathString);

        checkboxSelect(checkStatus, webElement);
    }

    public void checkboxSelect(final boolean checkStatus, final WebElement webElement)
    {
        logText((checkStatus ? "C" : "Unc") + "heck following checkbox: [{}].", getWebElementXPath(webElement));

        try
        {
            if(checkStatus)
            {
                if(!webElement.isSelected())
                    webElement.click();
            }
            else
            {
                if(webElement.isSelected())
                    webElement.click();
            }
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException | ElementNotVisibleException exception)
        {
            LOG.warn("Checkbox select Error: [{}] exception occurred during populating element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}].\n" +
                            "Printing StackTrace below:\n",
                    exception.getClass().getSimpleName(), webElement.toString(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName(),
                    exception.toString());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();
        }

        LOG.info((checkStatus ? "C" : "Unc") + "hecked.");

        waitForPageReady(getCurrentWebDriver());
    }


    /***************************************************************
     *  CLEAR ELEMENT
     */

    public void clearByXPath(final String xpathString)
    {
        final WebElement webElement = findSingleElementByXPath("visible", xpathString);

        clear(webElement);
    }

    public void clear(final WebElement webElement)
    {
        logText("Clear following element: [{}].", getWebElementXPath(webElement));

        try
        {
            webElement.clear();
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException | ElementNotVisibleException exception)
        {
            LOG.warn("Populate Error: [{}] exception occurred during populating element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}].\n" +
                            "Printing StackTrace below:\n",
                    exception.getClass().getSimpleName(), webElement.toString(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName(),
                    exception.toString());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();
        }

        LOG.info("Populated.");

        waitForPageReady(getCurrentWebDriver());
    }

    /***************************************************************
     *  CLICK
     */

    public void clickByXPath(final String xPathString)
    {
        WebElement webElement = findSingleElementByXPath("clickable", xPathString);

        click(webElement);
    }

    public void click(final WebElement webElement)
    {
        logText("Left click on element: {}", getWebElementXPath(webElement));

        try
        {
            webElement.click();
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException | ElementNotVisibleException exception)
        {
            logWarn("Click Error: [{}] exception occurred waiting for presence of element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}].\n" +
                            "Printing StackTrace below:\n",
                    exception.getClass().getSimpleName(), webElement.toString(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName(),
                    exception.toString());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();
        }

        LOG.info("Clicked.");

        waitForPageReady(getCurrentWebDriver());
    }


    /***************************************************************
     *  DOUBLE CLICK
     */

    public void doubleClickByXPath(final String xPathString)
    {
        WebElement webElement = findSingleElementByXPath("clickable", xPathString);

        doubleClick(webElement);
    }

    public void doubleClick(final WebElement webElement)
    {
        logText("Double Left click on element: {}", getWebElementXPath(webElement));

        try
        {
            Actions actions = new Actions(getCurrentWebDriver());
            actions.doubleClick(webElement);
            actions.build().perform();
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException | ElementNotVisibleException exception)
        {
            LOG.warn("Click Error: [{}] exception occurred waiting for presence of element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}].\n" +
                            "Printing StackTrace below:\n",
                    exception.getClass().getSimpleName(), webElement.toString(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName(),
                    exception.toString());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();
        }

        LOG.info("Double clicked.");

        waitForPageReady(getCurrentWebDriver());
    }


    /***************************************************************
     *  GET
     */

    public String getBrowserTitle()
    {
        return getCurrentWebDriver().getTitle();
    }

    public String getWebElementXPath(final WebElement webElement)
    {
        final String[] elements = webElement.toString().split("xpath: ");

        if(elements.length > 1)
        {
            final String str = elements[1];

            return str.substring(0, str.length() - 1);
        }

        return webElement.toString();
    }

    public String getElementAttributeValueByXPath(final String attrName, final String xPath)
    {
        return findSingleElementByXPath("present", xPath).getAttribute(attrName);
    }


    /***************************************************************
     *  HOVER
     */

    public void hoverOverByXPath(final String xPath)
    {
        waitForElementsToReload(2000);

        WebElement hoverElementPresent = findSingleElementByXPath("present", xPath);

        // Scroll window so the element is visible
        scrollToViewElement(hoverElementPresent);

        WebElement hoverElement = findSingleElementByXPath("visible", xPath);

        hoverOver(hoverElement);
    }

    public void hoverOver(WebElement hoverElement)
    {
        logText("Hover Over the following element: {}", getWebElementXPath(hoverElement));

        try
        {
            Actions actions = new Actions(getCurrentWebDriver());
            actions.moveToElement(hoverElement);
            actions.build().perform();
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            LOG.warn("Hover Error: [{}] exception occurred waiting for presence of element on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}]",
                    exception.getClass().getSimpleName(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();

            throw exception;
        }

        LOG.info("Hovered.");

        waitForPageReady(getCurrentWebDriver());
    }


    /***************************************************************
     *  LOG
     */

    public void logBrowserInfo()
    {
        // Set window position so the element would be on the center
        String currentPosition = getCurrentWebDriver().manage().window().getPosition().toString();
        LOG.info("Current Position: {}", currentPosition);

        String currentSize = getCurrentWebDriver().manage().window().getSize().toString();
        LOG.info("Current Size: {}", currentSize);
    }

    public void logStep(String s, Object... objects)
    {
        LOG.info("");
        LOG.info(NEO_STEP_PREFIX + s, objects);
    }

    public void logText(String s, Object... objects)
    {
        LOG.info(s, objects);
    }

    public void logWarn(String s, Object... objects)
    {
        LOG.warn(s, objects);
    }


    /***************************************************************
     *  POPULATE
     */

    public void populateByNeo(final String inputText, final String... dataID)
    {
        final WebElement webElement = findSingleElementByNeo("visible", dataID);

        populate(inputText, webElement);
    }

    public void populateByXPath(final String inputText, final String xpathString)
    {
        final WebElement webElement = findSingleElementByXPath("visible", xpathString);

        populate(inputText, webElement);
    }

    public void populate(final String inputText, final WebElement webElement)
    {
        logText("Populate following element [{}] with [{}] text.", getWebElementXPath(webElement), inputText);

        try
        {
            scrollToViewElement(webElement); // This is try how this would work:)

            webElement.clear();
            webElement.sendKeys(inputText);
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException | ElementNotVisibleException exception)
        {
            LOG.warn("Populate Error: [{}] exception occurred during populating element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}].\n" +
                            "Printing StackTrace below:\n",
                    exception.getClass().getSimpleName(), webElement.toString(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName(),
                    exception.toString());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();
        }

        LOG.info("Populated.");

        waitForPageReady(getCurrentWebDriver());
    }


    /***************************************************************
     *  RADIO SELECT
     */

    public void radioSelectByNeo(final String... dataID)
    {
        final WebElement webElement = findSingleElementByNeo("clickable", dataID);

        radioSelect(webElement);
    }

    public void radioSelectByXPath(final String xPathString)
    {
        final WebElement webElement = findSingleElementByXPath("clickable", xPathString);

        radioSelect(webElement);
    }

    public void radioSelect(final WebElement webElement)
    {
        logText("Select radio button: {}", getWebElementXPath(webElement));

        try
        {
            webElement.click();
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException | ElementNotVisibleException exception)
        {
            LOG.warn("Radio Select Error: [{}] exception occurred during populating element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}].\n" +
                            "Printing StackTrace below:\n",
                    exception.getClass().getSimpleName(), webElement.toString(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName(),
                    exception.toString());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();
        }

        LOG.info("Radio button selected.");

        waitForPageReady(getCurrentWebDriver());
    }

    public void radioSelectUsingValueByNeo(final String radioValue, final String... dataID)
    {
        final List<WebElement> radioGroup = findMultipleElementsByNeo("clickable", dataID);

        radioSelectUsingValue(radioValue, radioGroup);
    }

    public void radioSelectUsingValueByXPath(final String radioValue, final String xPathString)
    {
        final List<WebElement> radioGroup = findMultipleElementsByXPath("clickable", xPathString);

        radioSelectUsingValue(radioValue, radioGroup);
    }

    public void radioSelectUsingValue(final String radioValue, final List<WebElement> radioGroup)
    {
        logText("Select [{}] radio button using name attribute.", radioValue);

        try
        {
            for(final WebElement radio : radioGroup)
            {
                if(radio.getAttribute("value").equals(radioValue))
                {
                    if(!radio.isSelected())
                    {
                        radio.click();
                    }

                    break;
                }
            }
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException | ElementNotVisibleException exception)
        {
            LOG.warn("Radio Select Error: [{}] exception occurred during populating element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}].\n" +
                            "Printing StackTrace below:\n",
                    exception.getClass().getSimpleName(), radioGroup.toString(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName(),
                    exception.toString());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();
        }

        LOG.info("Radio button selected.");

        waitForPageReady(getCurrentWebDriver());
    }

    /***************************************************************
     *  SCROLL
     */

    public void scrollTo(final int xOffset, final int yOffset)
    {
        ((JavascriptExecutor) getCurrentWebDriver()).executeScript("window.scrollTo(arguments[0],arguments[1]);", xOffset, yOffset);

        LOG.info("Scrolled to {}, {}.", Integer.toString(xOffset), Integer.toString(yOffset));

        waitForPageReady(getCurrentWebDriver());
    }

    public void scrollToViewElement(final WebElement element)
    {
        int offset = getScrollOffsetY(element);

        ((JavascriptExecutor) getCurrentWebDriver()).executeScript("window.scrollTo(0,arguments[0]);", offset);

        LOG.info("Scrolled to 0, {}.", Integer.toString(offset));

        waitForPageReady(getCurrentWebDriver());
    }

    public void scrollToTop()
    {
        ((JavascriptExecutor) getCurrentWebDriver()).executeScript("window.scrollTo(0,0);");

        LOG.info("Scrolled to top.");

        waitForPageReady(getCurrentWebDriver());
    }

    // INTERNAL - DO NOT USE
    public int getScrollOffsetY(final WebElement element)
    {
        final int pageHeight = Integer.parseInt(runJavaScript("return document.documentElement.clientHeight"));
        final int elementLocationY = element.getLocation().getY();
        final int elementHeight = element.getSize().getHeight();

        final int offset = elementLocationY - Math.round(pageHeight / 2) + Math.round(elementHeight / 2);

        return (offset < 0 ? 0 : offset);
    }


    /***************************************************************
     *  SELECT
     */

    public void selectUsingTextByNeo(final String selectText, final String... dataID)
    {
        WebElement selectElement = findSingleElementByNeo("present", dataID);

        selectUsingText(selectText, selectElement);
    }

    public void selectUsingTextByXPath(final String selectText, final String xPath)
    {
        WebElement selectElement = findSingleElementByXPath("present", xPath);

        selectUsingText(selectText, selectElement);
    }

    public void selectUsingText(final String selectText, final WebElement webElement)
    {
        logText("Select [{}] on WebElement: {}", selectText, getWebElementXPath(webElement));

        try
        {
            Select realSelectElement = new Select(webElement);
            realSelectElement.selectByVisibleText(selectText);
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            LOG.warn("Select Error: [{}] exception occurred waiting for presence of element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}]",
                    exception.getClass().getSimpleName(), webElement.toString(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();

            throw exception;
        }

        waitForPageReady(getCurrentWebDriver());
    }

    public void selectUsingValueByNeo(final String optionValue, final String... dataID)
    {
        WebElement selectElement = findSingleElementByNeo("present", dataID);

        selectUsingValue(optionValue, selectElement);
    }

    public void selectUsingValueByXPath(final String optionValue, final String xPath)
    {
        WebElement selectElement = findSingleElementByXPath("present", xPath);

        selectUsingValue(optionValue, selectElement);
    }

    public void selectUsingValue(final String optionValue, final WebElement webElement)
    {
        logText("Select [{}] on following element {}.", optionValue, getWebElementXPath(webElement));

        try
        {
            Select realSelectElement = new Select(webElement);
            realSelectElement.selectByValue(optionValue);
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            LOG.warn("Select Error: [{}] exception occurred waiting for presence of element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}]",
                    exception.getClass().getSimpleName(), webElement.toString(), getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();

            throw exception;
        }

        waitForPageReady(getCurrentWebDriver());
    }


    /***************************************************************
     *  FIND SINGLE ELEMENT
     */

    public WebElement findSingleElementByNeo(final String stateType, final String... dataID)
    {
        final String xpathString = neoXPathBuilder(dataID);

        return findSingleElementByXPath(stateType, xpathString);
    }

    public WebElement findSingleElementByXPath(final String stateType, final String xpathString) throws TimeoutException,
            StaleElementReferenceException, NoSuchElementException
    {
        logText("Find single element by [{}].", xpathString);

        final WebDriver currWebDriver = getCurrentWebDriver();
        WebElement element;

        try
        {
            findElementTimer.reset().start();

            LOG.info("Searching for single element: {}", xpathString);

            WebDriverWait driverWait = new WebDriverWait(currWebDriver, NEO_WAIT_NORMAL_TIMEOUT);

            switch (stateType)
            {
                default: case "": case "present":
                element = driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathString)));
                break;
                case "visible":
                    element = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathString)));
                    break;
                case "clickable":
                    element = driverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathString)));
                    break;
            }

            findElementTimer.stop();

            LOG.info("Founded in {}.", findElementTimer);

            return element;
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            LOG.warn("[{}] exception occurred waiting for presence of element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}]",
                    exception.getClass().getSimpleName(), xpathString, getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();

            throw exception;
        }
    }


    /***************************************************************
     *  FIND MULTIPLE ELEMENTS
     */

    public List<WebElement> findMultipleElementsByNeo(final String stateType, final String... dataID)
    {
        return findMultipleElementsByXPath(stateType, neoXPathBuilder(dataID));
    }

    public List<WebElement> findMultipleElementsByXPath(final String stateType, final String xpathString) throws TimeoutException,
            StaleElementReferenceException, NoSuchElementException
    {
        logText("Searching for multiple elements: {}", xpathString);

        WebDriver currWebDriver = getCurrentWebDriver();
        List<WebElement> elementsList;

        try
        {
            WebDriverWait driverWait = new WebDriverWait(currWebDriver, NEO_WAIT_NORMAL_TIMEOUT);
            switch (stateType)
            {
                default:
                case "":
                case "present":
                    elementsList = driverWait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpathString)));
                    break;
                case "visible":
                    elementsList = driverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpathString)));
                    break;
            }

            if (elementsList.size() < 1)
            {
                LOG.warn("Element not found.");
                return null;
            }
            else if (elementsList.size() >= 1)
            {
                LOG.info("Found [{}] elements.", elementsList.size());
                return elementsList;
            }
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            LOG.warn("[{}] exception occurred waiting for presence of element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}]",
                    exception.getClass().getSimpleName(), xpathString, getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();

            throw exception;
        }

        return elementsList;
    }


    /***************************************************************
     *  FIND SINGLE ELEMENT UNDER ANOTHER ELEMENT
     */

    public WebElement findSingleElementUnderElementByNeo(final WebElement element, final String stateType, final String... dataID)
    {
        final String xpathString = neoXPathBuilder(dataID);

        return findSingleElementUnderElementByXpath(element, stateType, xpathString);
    }

    public WebElement findSingleElementUnderElementByXpath(final WebElement element, final String stateType, final String xpathString) throws TimeoutException,
            StaleElementReferenceException, NoSuchElementException
    {
        LOG.info("Searching for element: [{}]", xpathString);

        WebElement foundElement;

        try
        {
            findElementTimer.reset().start();

            WebDriverWait driverWait = new WebDriverWait(getCurrentWebDriver(), NEO_WAIT_NORMAL_TIMEOUT);

            switch (stateType)
            {
                default:
                case "":
                case "present":
                    foundElement = driverWait.until(presenceOfElementLocatedUnderWebElement(element, By.xpath(xpathString)));
                    break;
                case "visible":
                    foundElement = driverWait.until(visibilityOfElementLocatedUnderWebElement(element, By.xpath(xpathString)));
                    break;
                case "clickable":
                    foundElement = driverWait.until(clickableOfElementLocatedUnderWebElement(element, By.xpath(xpathString)));
                    break;
            }

            findElementTimer.stop();

            LOG.info("Founded in {}.", findElementTimer);

            return foundElement;
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            LOG.warn("[{}] exception occurred waiting for presence of element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}]",
                    exception.getClass().getSimpleName(), xpathString, getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();

            throw exception;
        }
    }


    /***************************************************************
     *  FIND MULTIPLE ELEMENTS UNDER ANOTHER ELEMENT
     */

    public List<WebElement> findMultipleElementsUnderElementByNeo(final WebElement element, final String stateType, final String... dataID)
    {
        final String xpathString = neoXPathBuilder(dataID);

        return findMultipleElementsUnderElementByXpath(element, stateType, xpathString);
    }

    public List<WebElement> findMultipleElementsUnderElementByXpath(final WebElement element, final String stateType, final String xpathString) throws TimeoutException,
            StaleElementReferenceException, NoSuchElementException
    {
        final WebDriver currWebDriver = getCurrentWebDriver();
        List<WebElement> foundElements;

        try
        {
            findElementTimer.reset().start();

            LOG.info("Searching for element: [{}]", xpathString);

            WebDriverWait driverWait = new WebDriverWait(currWebDriver, NEO_WAIT_NORMAL_TIMEOUT);

            switch (stateType)
            {
                default:
                case "":
                case "present":
                    foundElements = driverWait.until(presenceOfAllElementsLocatedUnderWebElement(element, By.xpath(xpathString)));
                    break;
                case "visible":
                    foundElements = driverWait.until(visibilityOfAllElementsLocatedUnderWebElement(element, By.xpath(xpathString)));
                    break;
                case "clickable":
                    foundElements = driverWait.until(clickableOfAllElementsLocatedUnderWebElement(element, By.xpath(xpathString)));
                    break;
            }

            findElementTimer.stop();

            LOG.info("Founded in {}.", findElementTimer);

            return foundElements;
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            LOG.warn("[{}] exception occurred waiting for presence of element [{}] on current page [{}}.\n" +
                            "Taking screen shot of [{}] for current test [{}]",
                    exception.getClass().getSimpleName(), xpathString, getCurrentWebDriver().getCurrentUrl(),
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName());

            getReportingFactory().takeScreenShot();
            getReportingFactory().writeHTMLOfCurrentPageToFile();

            throw exception;
        }
    }


    /***************************************************************
     *  EXPECTED CONDITIONS - INTERNAL - DO NOT USE
     */

    public ExpectedCondition<WebElement> presenceOfElementLocatedUnderWebElement(final WebElement webElement, final By by)
    {
        return new ExpectedCondition<WebElement>()
        {
            public WebElement apply(WebDriver driver)
            {
                return webElement.findElement(by);
            }
        };
    }

    public ExpectedCondition<WebElement> visibilityOfElementLocatedUnderWebElement(final WebElement webElement, final By by)
    {
        return new ExpectedCondition<WebElement>()
        {
            public WebElement apply(WebDriver driver)
            {
                WebElement element = webElement.findElement(by);
                return element.isDisplayed() ? element : null;
            }
        };
    }

    public ExpectedCondition<WebElement> clickableOfElementLocatedUnderWebElement(final WebElement webElement, final By by)
    {
        return new ExpectedCondition<WebElement>()
        {
            public ExpectedCondition<WebElement> visibilityOfElementLocatedUnderWebElement =
                    visibilityOfElementLocatedUnderWebElement(webElement, by);

            public WebElement apply(WebDriver driver)
            {
                WebElement element = visibilityOfElementLocatedUnderWebElement.apply(driver);
                if (element != null && element.isEnabled())
                {
                    return element;
                }
                else
                {
                    return null;
                }
            }
        };
    }

    public ExpectedCondition<List<WebElement>> presenceOfAllElementsLocatedUnderWebElement(final WebElement webElement, final By by)
    {
        return new ExpectedCondition<List<WebElement>>()
        {
            public List<WebElement> apply(WebDriver driver)
            {
                return webElement.findElements(by);
            }
        };
    }

    public ExpectedCondition<List<WebElement>> visibilityOfAllElementsLocatedUnderWebElement(final WebElement webElement, final By by)
    {
        return new ExpectedCondition<List<WebElement>>()
        {
            public List<WebElement> apply(WebDriver driver)
            {
                List<WebElement> elements = webElement.findElements(by);
                boolean elementsAreDisplayed = true;

                for(final WebElement singleElement: elements)
                {
                    if(!singleElement.isDisplayed())
                    {
                        elementsAreDisplayed = false;
                        break;
                    }
                }

                return elementsAreDisplayed ? elements : null;
            }
        };
    }

    public ExpectedCondition<List<WebElement>> clickableOfAllElementsLocatedUnderWebElement(final WebElement webElement, final By by)
    {
        return new ExpectedCondition<List<WebElement>>()
        {
            public ExpectedCondition<List<WebElement>> visibilityOfAllElementsLocatedUnderWebElement =
                    visibilityOfAllElementsLocatedUnderWebElement(webElement, by);

            public List<WebElement> apply(WebDriver driver)
            {
                List<WebElement> elements = visibilityOfAllElementsLocatedUnderWebElement.apply(driver);
                boolean elementsAreClickable = true;

                for(final WebElement singleElement : elements)
                {
                    if(!(singleElement != null && singleElement.isEnabled()))
                    {
                        elementsAreClickable = false;
                        break;
                    }
                }

                return elementsAreClickable ? elements : null;
            }
        };
    }


    /***************************************************************
     *  IS ELEMENT PRESENT
     */

    public boolean isElementPresentByNeo(final String... dataID)
    {
        final String xpathString = neoXPathBuilder(dataID);

        return isElementPresentByXPath(xpathString);
    }

    public boolean isElementPresentByXPath(final String xPath)
    {
        logText("Check if element is present [{}].", xPath);

        try
        {
            findElementTimer.reset().start();

            getCurrentWebDriver().findElement(By.xpath(xPath));

            findElementTimer.stop();

            logText("Element is present. Found in [{}].", findElementTimer);

            return true;
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            findElementTimer.stop();

            logText("Element is NOT present. Searched within [{}].", findElementTimer);

            return false;
        }
    }

    // TO DO - need to make couple tests what method is more efficient. Implicit wait as above or Timeout as below.
    public boolean isElementPresentByXPath2(final String xPath)
    {
        logText("Check if element is present [{}].", xPath);

        try
        {
            findElementTimer.reset().start();

            WebDriverWait driverWait = new WebDriverWait(getCurrentWebDriver(), NEO_WAIT_SHORT_TIMEOUT);

            driverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xPath)));

            findElementTimer.stop();

            logText("Element is present. Found in [{}].", findElementTimer);

            return true;
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            findElementTimer.stop();

            logText("Element is NOT present. Searched within [{}].", findElementTimer);

            return false;
        }
    }

    /***************************************************************
     *  ARE ELEMENTS PRESENT
     */

    public boolean areElementsPresentByNeo(final String... dataID)
    {
        final String xpathString = neoXPathBuilder(dataID);

        return areElementsPresentByXPath(xpathString);
    }

    public boolean areElementsPresentByXPath(final String xPath)
    {
        logText("Check if elements are present [{}].", xPath);

        try
        {
            final List<WebElement> webElements = getCurrentWebDriver().findElements(By.xpath(xPath));

            if (webElements != null && webElements.size() != 0)
            {
                LOG.info("Elements are present.");

                return true;
            }

            LOG.info("Elements are NOT present.");

            return false;
        }
        catch (NoSuchElementException exception)
        {
            LOG.info("Elements are NOT present.");

            return false;
        }
    }

    /***************************************************************
     *  IS ELEMENT DISPLAYED
     */

    public boolean isElementDisplayedByNeo(final String... dataID)
    {
        return isElementPresentByNeo(dataID) && isElementDisplayed(findSingleElementByNeo("present", dataID));
    }

    public boolean isElementDisplayedByXPath(final String xPath)
    {
        return isElementPresentByXPath(xPath) && isElementDisplayed(findSingleElementByXPath("present", xPath));
    }

    public boolean isElementDisplayed(final WebElement webElement)
    {
        if (webElement.isDisplayed())
        {
            LOG.info("Element is displayed.");

            return true;
        }
        else
        {
            LOG.info("Element is NOT displayed.");

            return false;
        }
    }


    /***************************************************************
     *  IS ELEMENT NOT EMPTY
     */

    public boolean isElementEmptyByNeo(final boolean emptyStatus, final String... dataID)
    {
        return isElementEmpty(findSingleElementByNeo("present", dataID));
    }

    public boolean isElementEmptyByXPath(final String xPath)
    {
        return isElementEmpty(findSingleElementByXPath("present", xPath));
    }

    public boolean isElementEmpty(final WebElement webElement)
    {
        if (webElement.getText().isEmpty())
        {
            logText("Element is empty.");

            return true;
        }
        else
        {
            logText("Element is NOT empty. It contains: [{}].", webElement.getText());

            return false;
        }
    }


    /***************************************************************
     *  IS TEXT PRESENT IN ELEMENT
     */

    public boolean isTextPresentInElementByNeo(final String text, final String xPath)
    {
        return isTextPresentInElementByXPath(text, neoXPathBuilder(xPath));
    }

    public boolean isTextPresentInElementByXPath(final String text, final String xPath)
    {
        LOG.info("Check if [{}] text is present in element [{}].", text, xPath);

        try
        {
            findElementTimer.reset().start();

            WebDriverWait wait = new WebDriverWait(getCurrentWebDriver(), NEO_WAIT_MEDIUM_TIMEOUT);

            wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(xPath), text));

            findElementTimer.stop();

            logText("Text [{}] is present in element [{}]. Checked in [{}].", text, xPath, findElementTimer);

            return true;
        }
        catch (final TimeoutException | StaleElementReferenceException | NoSuchElementException exception)
        {
            findElementTimer.stop();

            logText("Text [{}] is NOT present in element [{}]. Checked in [{}].", text, xPath, findElementTimer);

            return false;
        }
    }

    /***************************************************************
     *  WAIT METHODS
     */

    public void waitForPageReady(final WebDriver driver)
    {
        waitForPageReady(driver, PAGE_LOAD_TIMEOUT);
    }

    public void waitForPageReady(final WebDriver driver, final int timeout)
    {
        pageLoadTimer.reset().start();

        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };

        Wait<WebDriver> wait = new WebDriverWait(driver, timeout);

        try
        {
            wait.until(expectation);

            pageLoadTimer.stop();

            LOG.info("waitForPageReady: Page was reloaded in {} on [{}].", pageLoadTimer, driver.getCurrentUrl());
        }
        catch(final Exception exception)
        {
            pageLoadTimer.stop();

            LOG.warn("ERROR: waitForPageReady(): Timeout of [{}] seconds occurred during waiting for Page Load Request to complete.\n" +
                            "Taking screen shot of [{}] for current test [{}]",
                    timeout,
                    getReportingFactory().getPageName(),
                    getReportingFactory().getCurrentTestName());

            getReportingFactory().takeScreenShot("","waitForPageLoad");
            getReportingFactory().writeHTMLOfCurrentPageToFile();

            throw exception;
        }
    }

    public void waitForElementsToReload(final int timeInMs)
    {
        try
        {
            LOG.info("Sleep for [{}] seconds to wait for page [{}] to refresh.", timeInMs/1000, getCurrentWebDriver().getCurrentUrl());

            Thread.sleep(timeInMs);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    // More tests and prototypes needed
    public WebElement fluentWait(WebDriver driver, final By locator)
    {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(FLUENT_WAIT_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(FLUENT_WAIT_POOLING_TIME, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement foundElement = wait.until(new Function<WebDriver, WebElement>()
        {
            @Override
            public WebElement apply(WebDriver driver)
            {
                return driver.findElement(locator);
            }
        });

        return  foundElement;
    }

    /***************************************************************
     *  OTHER
     */

    protected String runJavaScript(final String js)
    {
        Object result = ((JavascriptExecutor)getCurrentWebDriver()).executeScript(js);

        return result == null ? "0" : result.toString();
    }


    /***************************************************************
     * CHECKPOINTS
     */

    public void verifyBrowserPageTitleContains(final String text)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        verifyCheckpoint.setDescription("Verify the text [%s] is present in Browser Title on current page [%s]",
                text, getCurrentWebDriver().getCurrentUrl());

        String currentBrowserTitle = getBrowserTitle();

        verifyCheckpoint.setPassMessage("Following text [%s] is present in Browser Title on current page [%s].",
                text, getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setFailMessage("Following text [%s] is NOT present in Browser Title on current page [%s]. Current Browser Title is [%s]",
                text, getCurrentWebDriver().getCurrentUrl(), currentBrowserTitle);

        verifyCheckpoint.execute(currentBrowserTitle.contains(text));
    }

    // ---------------------------------------------------------------------

    public void verifyElementAttributeContainsText(final String tagName, final String tagAttributeName, final String tagAttributeValue)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        verifyCheckpoint.setDescription("Verify the text [%s] is present in [%s] attribute of the tag [%s] on current page [%s]",
                tagAttributeValue, tagAttributeName, tagName, getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setPassMessage("Following text [%s] is present in [%s] attribute of the tag [%s].",
                tagAttributeValue, tagAttributeName, tagName);
        verifyCheckpoint.setFailMessage("Following text [%s] is NOT present in [%s] attribute of the tag [%s].",
                tagAttributeValue, tagAttributeName, tagName);

        verifyCheckpoint.execute(findSingleElementByNeo("present", tagName + ";").getAttribute(tagAttributeName).contains(tagAttributeValue));
    }

    // ---------------------------------------------------------------------

    // TO DO
    public void verifyElementContainsAttributeByXPath(final String expectedAttributeName, final String elementXPath)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        verifyCheckpoint.setDescription("Verify the [%s] element contains [%s] attribute on current page [%s]",
                elementXPath, expectedAttributeName, getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setPassMessage("Element [%s] contains [%s] attribute.",
                elementXPath, expectedAttributeName);
        verifyCheckpoint.setFailMessage("Element [%s] does NOT contain [%s] attribute.",
                elementXPath, expectedAttributeName);

        verifyCheckpoint.execute(isElementPresentByXPath(elementXPath + "[@" + expectedAttributeName + "]"));
    }

    public void verifyElementDoesNotContainAttributeByXPath(final String expectedAttributeName, final String elementXPath)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        verifyCheckpoint.setDescription("Verify the [%s] element does NOT contain [%s] attribute on current page [%s]",
                elementXPath, expectedAttributeName, getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setPassMessage("Element [%s] does NOT contain [%s] attribute.",
                elementXPath, expectedAttributeName);
        verifyCheckpoint.setFailMessage("Element [%s] contains [%s] attribute.",
                elementXPath, expectedAttributeName);

        verifyCheckpoint.execute(!isElementPresentByXPath(elementXPath + "[@" + expectedAttributeName + "]"));
    }

    // ---------------------------------------------------------------------
    // Verify Element Contains Text

    public void verifyElementContainsTextByNeo(final String expectedText, final String... dataID)
    {
        final WebElement webElement = findSingleElementByNeo("present", dataID);

        verifyElementContainsText(expectedText, webElement);
    }

    public void verifyElementContainsTextByXPath(final String expectedText, final String elementXPath)
    {
        final WebElement webElement = findSingleElementByXPath("present", elementXPath);

        verifyElementContainsText(expectedText, webElement);
    }

    public void verifyElementContainsText(final String expectedText, final WebElement webElement)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        verifyCheckpoint.setDescription("Verify that [%s] element contains [%s] text on page [%s].",
                getWebElementXPath(webElement), expectedText, getCurrentWebDriver().getCurrentUrl());

        final String actualText = webElement.getText();

        verifyCheckpoint.setPassMessage("Element [%s] contains [%s] text.",
                getWebElementXPath(webElement), expectedText);
        verifyCheckpoint.setFailMessage("Element [%s] does NOT contain [%s] text. Actual = [%s].",
                getWebElementXPath(webElement), expectedText, actualText);

        verifyCheckpoint.execute(actualText.contains(expectedText));
    }

    // ---------------------------------------------------------------------
    // Verify Element Is Displayed

    public void verifyElementIsDisplayedByNeo(final String... dataID)
    {
        final WebElement webElement = findSingleElementByNeo("present", dataID);

        verifyElementIsDisplayedAndEnabled(webElement);
    }

    public void verifyElementIsDisplayedByXPath(final String xPath)
    {
        final WebElement webElement = findSingleElementByXPath("present", xPath);

        verifyElementIsDisplayedAndEnabled(webElement);
    }

    public void verifyElementIsDisplayed(final WebElement webElement)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        verifyCheckpoint.setDescription("Verify that [%s] is displayed on current page [%s].",
                getWebElementXPath(webElement), getCurrentWebDriver().getCurrentUrl());

        verifyCheckpoint.setPassMessage("[%s] is displayed on current page.", getWebElementXPath(webElement));
        verifyCheckpoint.setFailMessage("[%s] is NOT displayed on current page.", getWebElementXPath(webElement));

        verifyCheckpoint.execute(webElement.isDisplayed());
    }

    // ---------------------------------------------------------------------
    // Verify Element Is Displayed And Enabled

    public void verifyElementIsDisplayedAndEnabledByNeo(final String... dataID)
    {
        final WebElement webElement = findSingleElementByNeo("present", dataID);

        verifyElementIsDisplayedAndEnabled(webElement);
    }

    public void verifyElementIsDisplayedAndEnabledByXPath(final String xPath)
    {
        final WebElement webElement = findSingleElementByXPath("present", xPath);

        verifyElementIsDisplayedAndEnabled(webElement);
    }

    public void verifyElementIsDisplayedAndEnabled(final WebElement webElement)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        verifyCheckpoint.setDescription("Verify that [%s] is present on current page [%s].",
                getWebElementXPath(webElement), getCurrentWebDriver().getCurrentUrl());


        verifyCheckpoint.setPassMessage("[%s] is displayed and enabled on current page.", getWebElementXPath(webElement));
        verifyCheckpoint.setFailMessage("[%s] is NOT displayed and enabled on current page.", getWebElementXPath(webElement));

        verifyCheckpoint.execute(webElement.isDisplayed() && webElement.isEnabled());
    }

    // ---------------------------------------------------------------------

    public void verifyLocalImagesAreLoaded()
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);
        int brokenImageCount = 0;
        int imageCount = 0;

        verifyCheckpoint.setDescription("Verify that all images on [%s] page are loaded.",
                getCurrentWebDriver().getCurrentUrl());

        // Read all links
        final List<WebElement> allImages = findMultipleElementsByNeo("present", "img;");

        LOG.info("Found [{}] images on [{}] page.", allImages.size(), getCurrentWebDriver().getCurrentUrl());

        // Check each image
        for (final WebElement image : allImages)
        {
            LOG.info("Checking image [{}/{}]: [{}].", ++imageCount, allImages.size(), image.getAttribute("src"));

            if (image.isDisplayed())
            {
                final boolean loaded = (Boolean) ((JavascriptExecutor)getCurrentWebDriver()).executeScript("return arguments[0].complete", image);
                if (!loaded)
                {
                    LOG.warn("Image [{}] not loaded on page [{}]", image.getAttribute("src"), getCurrentWebDriver().getCurrentUrl());
                    brokenImageCount++;
                }
            }
        }

        verifyCheckpoint.setPassMessage("All [%s] images on [%s] page are loaded.",
                allImages.size(), getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setFailMessage("There are [%s] NOT loaded images on [%s] page.",
                brokenImageCount, getCurrentWebDriver().getCurrentUrl());

        verifyCheckpoint.execute(brokenImageCount == 0);
    }

    // ---------------------------------------------------------------------

    public void verifyLocalLinksAreValid()
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        final String startingPoint = currentWebDriver.getCurrentUrl();
        final List<WebElement> allLinks = findMultipleElementsByNeo("present", "a;");
        List<String> linksToClick = Lists.newArrayList();
        int brokenPageCount = 0;
        int linkCount = 0;

        verifyCheckpoint.setDescription("Verify that all links on [%s] page are valid.",
                startingPoint);

        // Read all links
        for (final WebElement link : allLinks)
        {
            if (link.getAttribute("href") != null &&
                    !link.getAttribute("href").isEmpty() &&
                    !link.getAttribute("href").contains("#"))
            {
                linksToClick.add(link.getAttribute("href"));
            }
        }

        //LOG.info("Found [{}] links.", linksToClick.size());

        // Navigate to each link
        for (final String link : linksToClick)
        {
            LOG.info("Navigate to link [{}/{}]: [{}]", ++linkCount, linksToClick.size(), link);

            pageLoadTimer.reset().start();

            getCurrentWebDriver().navigate().to(link);

            pageLoadTimer.stop();

            final String pageLoadTimerString = pageLoadTimer.elapsed(TimeUnit.SECONDS) + " sec";

            LOG.info("Page was loaded in [{}].", pageLoadTimerString);

            // Extend timeout for links that points outside the Ted Baker site.
            if (link.contains(getNeoTestWrapper().hostURL))
            {
                waitForPageReady(getCurrentWebDriver());
            }
            else
            {
                waitForElementsToReload(1000);  // Instagram doesn't like the speed of Selenium

                waitForPageReady(getCurrentWebDriver(), PAGE_LOAD_TIMEOUT_LONG);
            }

            if (getBrowserTitle().contains("Server Error") || getBrowserTitle().contains("Not Found"))
            {
                brokenPageCount++;
                LOG.warn("The page at [{}] does not exist.", link);
            }
        }

        getCurrentWebDriver().navigate().to(startingPoint);

        waitForPageReady(getCurrentWebDriver());

        verifyCheckpoint.setPassMessage("All [%s] links on [%s] page are valid.",
                linksToClick.size(), getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setFailMessage("There are [%s] invalid links on [%s] page.",
                brokenPageCount, getCurrentWebDriver().getCurrentUrl());

        verifyCheckpoint.execute(brokenPageCount == 0);
    }

    // ---------------------------------------------------------------------

    public void verifyTextPresent(final String text)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        verifyCheckpoint.setDescription("Verifying the text [%s] is present on current page [%s]",
                text, getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setPassMessage("Following text [%s] is present on current page [%s].",
                text, getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setFailMessage("Following text [%s] is NOT present on current page [%s].",
                text, getCurrentWebDriver().getCurrentUrl());

        verifyCheckpoint.execute(isElementPresentByNeo("*;text():-:" + text));
    }

    public void verifyTextNotPresent(final String text)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        verifyCheckpoint.setDescription("Verifying the text [%s] is NOT present on current page [%s]",
                text, getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setPassMessage("Following text [%s] is NOT present on current page [%s].",
                text, getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setFailMessage("Following text [%s] is present on current page [%s].",
                text, getCurrentWebDriver().getCurrentUrl());

        verifyCheckpoint.execute(!isElementPresentByNeo("*;text():-:" + text));
    }


    // ---------------------------------------------------------------------
    // Verify Text Present In Section

    public void verifyTextPresentInSectionByNeo(final String text, final String... dataID)
    {
        verifyTextPresentInSection(text, findSingleElementByNeo("present", dataID));
    }

    public void verifyTextPresentInSectionByXPath(final String text, final String sectionXPath)
    {
        verifyTextPresentInSection(text, findSingleElementByXPath("present", sectionXPath));
    }

    public void verifyTextPresentInSection(final String text, final WebElement webElement)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        final String elementText = webElement.getText();

        verifyCheckpoint.setDescription("Verifying the text [%s] is present in [%s] on current page [%s]",
                text, getWebElementXPath(webElement), getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setPassMessage("Following text [%s] is present in [%s] on current page [%s].",
                text, getWebElementXPath(webElement), getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setFailMessage("Following text [%s] is NOT present in [%s] on current page [%s]. Section text = [%s]",
                text, getWebElementXPath(webElement), getCurrentWebDriver().getCurrentUrl(), elementText);

        verifyCheckpoint.execute(elementText.contains(text));
    }

    // ---------------------------------------------------------------------
    // Verify Text Not Present In Section

    public void verifyTextNotPresentInSectionByNeo(final String text, final String... dataID)
    {
        verifyTextNotPresentInSection(text, findSingleElementByNeo("present", dataID));
    }

    public void verifyTextNotPresentInSectionByXPath(final String text, final String sectionXPath)
    {
        verifyTextNotPresentInSection(text, findSingleElementByXPath("present", sectionXPath));
    }

    public void verifyTextNotPresentInSection(final String text, final WebElement webElement)
    {
        NeoCheckpoint verifyCheckpoint = new NeoCheckpoint(this);

        final String elementText = webElement.getText();

        verifyCheckpoint.setDescription("Verifying the text [%s] is NOT present in [%s] on current page [%s]",
                text, getWebElementXPath(webElement), getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setPassMessage("Following text [%s] is NOT present in [%s] on current page [%s].",
                text, getWebElementXPath(webElement), getCurrentWebDriver().getCurrentUrl());
        verifyCheckpoint.setFailMessage("Following text [%s] is present in [%s] on current page [%s]. Section text = [%s]",
                text, getWebElementXPath(webElement), getCurrentWebDriver().getCurrentUrl(), elementText);

        verifyCheckpoint.execute(!elementText.contains(text));
    }


    /**
     * Find the dynamic element wait until its visible
     *
     * @param by Element location found by css, xpath, id etc...
     **/

    public WebElement waitForExpectedElement(final By by) {
        return wait.until(visibilityOfElementLocated(by));
    }

    private ExpectedCondition<WebElement> visibilityOfElementLocated(final By by) throws NoSuchElementException {
        return driver -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                LOG.error(e.getMessage());
            }
            WebElement element = testWrapper.singleDriver.findElement(by);
            return element.isDisplayed() ? element : null;
        };
    }

    // ---------------------------------------------------------------------
}