package core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.System.out;

/**
 * Created by sreekanth.bongunuri on 09/11/15.
 */
public class LoadProperties {

    private static final Logger LOG = LoggerFactory.getLogger(LoadProperties.class);

    private static Properties environmentProps;

    private static Properties props;

    public static void loadConfigProperties(String configPropertyFileLocation){
        environmentProps = new Properties();
        try (InputStream inputStream = LoadProperties.class.getResourceAsStream(configPropertyFileLocation)) {
            environmentProps.load(inputStream);
            environmentProps.list(out);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        props = new Properties();
        try (InputStream inputStream = LoadProperties.class.getResourceAsStream(environmentProps.getProperty("profile.path"))) {
            props.load(inputStream);
            props.list(out);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }
}

