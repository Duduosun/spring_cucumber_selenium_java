package main.java.pages.cucumberPages;

import core.TestWrapper;
import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by sreekanth.bongunuri on 05/11/15.
 */

@Component
public class PostCodePage {

    @Autowired
    private TestWrapper testWrapper;

    public void checkPostCodeAvailable(){
        WebElement postCode = testWrapper.singleDriver.findElement(By.cssSelector("#post-19 > div > div:nth-of-type(2) > section > p:nth-of-type(2)"));
        Assert.assertTrue(postCode.getText().contains("EC2V 5DE"));
    }

}
