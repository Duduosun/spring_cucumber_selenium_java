package main.java.pages.cucumberPages;

import core.TestWrapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by sreekanth.bongunuri on 05/11/15.
 */

@Component
public class HomePage {

 @Autowired
  private TestWrapper testWrapper;

    private By contactUsLinkLocator = By.linkText("Contact Us");

    WebDriver driver;

    public WebDriver getDriver(){
        this.driver = testWrapper.singleDriver;
        return this.driver;
    }

    public void clickContactUsLink(){
        WebElement contactUsLink= getDriver().findElement(By.cssSelector("a[href='/contact/']"));
        contactUsLink.click();

    }

}
