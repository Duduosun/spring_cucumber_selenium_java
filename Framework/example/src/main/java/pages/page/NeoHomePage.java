package main.java.pages.page;

import core.TestWrapper;
import org.openqa.selenium.WebDriver;

import main.java.pages.page.NeoPageBody;

public class NeoHomePage extends NeoPageBody
{
    /***************************************************************
     * PAGE OBJECTS
     */


    /***************************************************************
     * PAGE CONSTRUCTOR
     */

    public NeoHomePage(WebDriver driver, TestWrapper neoTestWrapper)
    {
        super(driver, neoTestWrapper);
    }


    /***************************************************************
     * PAGE STEPS
     */


    /***************************************************************
     * CHECKPOINTS
     */


}