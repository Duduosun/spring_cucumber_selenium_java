package main.java.pages.page;

import core.NeoWebElement;
import core.PageWrapper;
import core.TestWrapper;
import org.openqa.selenium.WebDriver;

public class NeoPageBody extends PageWrapper
{
    /***************************************************************
     * PAGE OBJECTS
     */

    NeoWebElement headerSection = new NeoWebElement("default", "neo",
            "header;");

    NeoWebElement menuSection = new NeoWebElement("default", "neo",
            "ul;id::menu-enterprise");

    NeoWebElement contactUsLink = new NeoWebElement("default", "neo",
            "a;href::/contact/");
    //contactUsLink.addAncestors(headerSection, menuSection);

    NeoWebElement footerSection = new NeoWebElement("default", "neo",
            "footer;");

    // MOBILE

    NeoWebElement mMenuLink = new NeoWebElement("default", "neo",
            "div;class::mobile-menu-btn");


    /***************************************************************
     * PAGE CONSTRUCTOR
     */

    public NeoPageBody(WebDriver driver, TestWrapper neoTestWrapper)
    {
        super(driver, neoTestWrapper);
    }


    /***************************************************************
     * PAGE ACTIONS
     */

    public NeoContactUsPage clickMenuContactUs()
    {
        logStep("Left click on \"Men's\" department.");

        if(getNeoTestWrapper().mobileFlag) {
            clickByXPath(headerSection.getXPath() + mMenuLink.getXPath());
        }

        clickByXPath(headerSection.getXPath() + menuSection.getXPath() + contactUsLink.getXPath());

        return new NeoContactUsPage(getCurrentWebDriver(), getNeoTestWrapper());
    }

    /***************************************************************
     * CHECKPOINTS
     */


}