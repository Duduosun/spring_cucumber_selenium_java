package main.java.pages.page;

import core.NeoWebElement;
import core.TestWrapper;
import org.openqa.selenium.WebDriver;

import main.java.pages.page.NeoPageBody;

public class NeoContactUsPage extends NeoPageBody
{
    /***************************************************************
     * PAGE OBJECTS
     */

    NeoWebElement mainSection = new NeoWebElement("default", "neo",
            "div;id::main");

    NeoWebElement contactDetailsSection = new NeoWebElement("default", "neo",
            "h1;text()::Contact Details", "ancestor::section;");


    /***************************************************************
     * PAGE CONSTRUCTOR
     */

    public NeoContactUsPage(WebDriver driver, TestWrapper neoTestWrapper)
    {
        super(driver, neoTestWrapper);
    }


    /***************************************************************
     * PAGE STEPS
     */



    /***************************************************************
     * PAGE ACTIONS
     */



    /***************************************************************
     * CHECKPOINTS
     */

    public void verifyContactDetailsPostcode(String postcode)
    {
        verifyTextPresentInSectionByXPath(postcode, mainSection.getXPath() + contactDetailsSection.getXPath());
    }
}