package main.java.data;

//import com.neoworks.selenium.tb303.framework.core.NeoProduct;

public class DefaultData
{
    // Global Test Data
    public static String SITE_COUNTRY;

    public static String CS_USERNAME;
    public static String CS_PASSWORD;
    public static String HAC_USERNAME;
    public static String HAC_PASSWORD;
    public static String HMC_USERNAME;
    public static String HMC_PASSWORD;

    public static String USER_FIRSTNAME;
    public static String USER_LASTNAME;
    public static String USER_PASSWORD;
    public static String USER_GENDER_MALE;
    public static String USER_GENDER_FEMALE;
    public static String USER_COUNTRY;
    public static String USER_STORE;

    public static String USER_EMAIL;

    public static int NEW_USER_EMAIL_USERNAME_LENGTH;
    public static int NEW_USER_EMAIL_DOMAIN_LENGTH;

    // Test Address Data
    public static String STANDARD_DELIVERY_MODE;

    public static String DEL1_ADDR_FIRSTNAME;
    public static String DEL1_ADDR_LASTNAME;
    public static String DEL1_ADDR_PHONE;

    public static String DEL1_ADDR_LINE_1;
    public static String DEL1_ADDR_LINE_2;
    public static String DEL1_ADDR_LINE_3;
    public static String DEL1_ADDR_CITY;
    public static String DEL1_ADDR_STATE;
    public static String DEL1_ADDR_POSTCODE;
    public static String DEL1_ADDR_COUNTRY;

    public static String CC1_ADDR_LINE_1;
    public static String CC1_ADDR_LINE_2;
    public static String CC1_ADDR_LINE_3;
    public static String CC1_ADDR_CITY;
    public static String CC1_ADDR_POSTCODE;
    public static String CC1_ADDR_COUNTRY;
    public static String CC1_ADDR_PHONE;
    // END of Test Address Data

    // Test Payment Data
    public static String DATACASH_CARD_NUMBER_1;
    public static String DATACASH_CARD_NUMBER_2;
    public static String DATACASH_CARD_NUMBER_3;
    public static String DATACASH_CARD_NUMBER_4;
    public static String DATACASH_CARD_NUMBER_5;
    public static String DATACASH_CARD_NUMBER_6;
    public static String DATACASH_CARD_NUMBER_7;
    public static String DATACASH_CARD_NUMBER_8;
    public static String DATACASH_CARD_NUMBER_9;

    public static String BILLING_EXPIRY_MONTH;
    public static String BILLING_EXPIRY_YEAR;
    public static String BILLING_SECURITY_CODE;

    public static String BILLING2_EXPIRY_MONTH;
    public static String BILLING2_EXPIRY_YEAR;
    public static String BILLING2_SECURITY_CODE;
    // END of Test Payment Data

    // Global Test Products
    /*public static NeoProduct product1;
    public static NeoProduct product2;
    public static NeoProduct product3;*/

    public DefaultData()
    {
        loadTestData();
    }


    public DefaultData(final String site_country)
    {
        SITE_COUNTRY = site_country;

        loadTestData();
    }

    // load test data
    public void loadTestData()
    {
        testHybrisData();
        testUserData();
        testAddressData();
        testPaymentData();
        testProductData();
    }

    // Setup User test data
    public void testHybrisData()
    {
        CS_USERNAME = "csagent";
        CS_PASSWORD = "1234";

        HAC_USERNAME = "admin";
        HAC_PASSWORD = "nimda";

        HMC_USERNAME = "admin";
        HMC_PASSWORD = "nimda";

    }

    // Setup User test data
    public void testUserData()
    {
        USER_FIRSTNAME = "SelTestFN";
        USER_LASTNAME = "SelTestLN";
        USER_EMAIL = "selenium@neoworks.com";
        USER_PASSWORD = "neoworks";
        USER_GENDER_MALE = "MALE";
        USER_GENDER_FEMALE = "FEMALE";

        NEW_USER_EMAIL_USERNAME_LENGTH = 6;
        NEW_USER_EMAIL_DOMAIN_LENGTH = 6;
    }

    // Setup Address test data
    public void testAddressData()
    {
        // SHIPPING DETAILS
        DEL1_ADDR_FIRSTNAME = "SelTestFN";
        DEL1_ADDR_LASTNAME = "SelTestLN";
        DEL1_ADDR_PHONE = "123456789";

        // SITE DEPENDENT TEST DATA
        switch(SITE_COUNTRY)
        {
            case "us":
                USER_COUNTRY = "United States";
                USER_STORE = "BLOOMINGDALES - NY SOHO";         // This value is connected with DEL1_ADDR_POSTCODE

                STANDARD_DELIVERY_MODE = "STANDARD GROUND DELIVERY";

                DEL1_ADDR_LINE_1 = "100 US Street";
                DEL1_ADDR_LINE_2 = "";
                DEL1_ADDR_LINE_3 = "";
                DEL1_ADDR_CITY = "New York";
                DEL1_ADDR_STATE = "NY";
                DEL1_ADDR_POSTCODE = "10001";
                DEL1_ADDR_COUNTRY = "United States";
                break;
            case "ca":
                USER_COUNTRY = "Canada";
                USER_STORE = "TORONTO PREMIUM OUTLETS";         // This value is connected with DEL1_ADDR_POSTCODE

                STANDARD_DELIVERY_MODE = "STANDARD GROUND DELIVERY";

                DEL1_ADDR_LINE_1 = "100 CA Street";
                DEL1_ADDR_LINE_2 = "";
                DEL1_ADDR_LINE_3 = "";
                DEL1_ADDR_CITY = "Toronto";
                DEL1_ADDR_STATE = "ON";
                DEL1_ADDR_POSTCODE = "L7G 0J1";
                DEL1_ADDR_COUNTRY = "Canada";
                break;
            case "uk": default:
            USER_COUNTRY = "United Kingdom";
            USER_STORE = "TEDS GROOMING ROOM - HOLBORN";    // This value is connected with DEL1_ADDR_POSTCODE

            STANDARD_DELIVERY_MODE = "STANDARD DELIVERY";

            DEL1_ADDR_LINE_1 = "100 UK Street";
            DEL1_ADDR_LINE_2 = "";
            DEL1_ADDR_LINE_3 = "";
            DEL1_ADDR_CITY = "London";
            DEL1_ADDR_STATE = null;
            DEL1_ADDR_POSTCODE = "WC1V 6PL";
            DEL1_ADDR_COUNTRY = "United Kingdom";

            CC1_ADDR_LINE_1 = "9 -10 FLORAL STREET";
            CC1_ADDR_LINE_2 = "COVENT GARDEN";
            CC1_ADDR_LINE_3 = "";
            CC1_ADDR_CITY = "LONDON";
            CC1_ADDR_POSTCODE = "WC2E 9HW";
            CC1_ADDR_COUNTRY = "United Kingdom";
            CC1_ADDR_PHONE = "020 7836 7808";
            break;
        }
    }


    // Setup Test Payment Data
    public void testPaymentData()
    {
        // BILLING DETAILS
        DATACASH_CARD_NUMBER_1 = "4444333322221111";
        DATACASH_CARD_NUMBER_2 = "1000110000000005";
        DATACASH_CARD_NUMBER_3 = "1000120000000004";
        DATACASH_CARD_NUMBER_4 = "1000380000000012";
        DATACASH_CARD_NUMBER_5 = "1000380000000020";
        DATACASH_CARD_NUMBER_6 = "1000380000000038";
        DATACASH_CARD_NUMBER_7 = "1000171234567896";
        DATACASH_CARD_NUMBER_8 = "1000189853512019";
        DATACASH_CARD_NUMBER_9 = "1000380000000004";

        BILLING_EXPIRY_MONTH = "02";
        BILLING_EXPIRY_YEAR = "2016";
        BILLING_SECURITY_CODE = "123";

        BILLING2_EXPIRY_MONTH = "02";
        BILLING2_EXPIRY_YEAR = "2016";
        BILLING2_SECURITY_CODE = "123";
    }

    // Setup test data
    public void testProductData()
    {
        /*
        *   TEST PRODUCTS FOR TED BAKER
        *
        * ### Product 1 ###
        * GATZBYJ - Wool suit jacket
        *      102942-00-BLACK    (Default)
        *      102942-05-GREY
        *      102942-15-MID_BLUE
        *      102942-28-TAUPE
        *          In Stock (1000):    38 R (Default), 40 R, 44 R
        *          Out of Stock (0):   36 R (Default), 42 R
        *
        */
/*

        // Test Product 1
        product1 = new NeoProduct("102942","GATZBYJ","Wool suit jacket");
        product1.setCodes(
                "102942-00-BLACK",      // Default
                "102942-05-GREY",
                "102942-15-MID_BLUE",
                "102942-28-TAUPE"
        );
        product1.setColours(
                "Black",                // Default
                "Grey",
                "Mid Blue",
                "Taupe"
        );
        product1.setSizesInStock(
                "38 R",                 // Default
                "40 R",
                "44 R"
        );
        product1.setSizesOutOfStock(
                "36 R (out of stock)",  // Default
                "42 R (out of stock)"
        );
*/

        // END of TEST PRODUCTS
    }

    public void setTestCountry(final String new_country)
    {
        SITE_COUNTRY = new_country;
    }
}