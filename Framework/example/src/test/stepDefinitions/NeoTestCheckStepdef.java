package test.stepDefinitions;


import core.TestWrapper;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import main.java.pages.cucumberPages.HomePage;
import main.java.pages.cucumberPages.PostCodePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by sreekanth.bongunuri on 05/11/15.
 */
@ContextConfiguration("classpath:cucumber.xml")
public class NeoTestCheckStepdef {

    @Autowired
    private TestWrapper testWrapper;

    @Autowired
    private HomePage homePage;

    @Autowired
    private PostCodePage postCodePage;

    @Given("^I have navigated to neoworks home page$")
    public void i_have_navigated_to_neoworks_home_page()  {

      testWrapper.singleDriver.get("http://www.neoworks.com");
    }

    @When("^I click on contact us link$")
    public void i_click_on_contact_us_link() {
      homePage.clickContactUsLink();
    }

    @When("^check for post code is available$")
    public void check_for_post_code_is_available()  {

        postCodePage.checkPostCodeAvailable();
    }


}
