package test.testRunners.cucumberTestRunners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


/**
 * Created by sreekanth.bongunuri on 05/11/15.
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "example/src/test/resources/features", monochrome = true, plugin = {
        "pretty", "html:target/cucumber-report/runapiat",
        "json:target/cucumber-report/runapiat/cucumber.json"},glue="stepDefinitions")
public class RunCucumberTests {
}

