package test.testJourneys;

import core.PageWrapper;
import core.TestWrapper;
import main.java.data.DefaultData;
import org.junit.Test;
import main.java.pages.page.NeoContactUsPage;
import main.java.pages.page.NeoHomePage;

import static org.junit.Assert.assertTrue;

public class NeoTestCheck extends TestWrapper
{
    @Test
    public void verifyXPathBuilderTest()
    {
        // Initialize Test Data
        DefaultData defaultData = new DefaultData(testCountry);

        // Test Data
        String s;   // String
        String resultXPath;   // Result

        // ---------------------------------------------------------------------
        // neoXPathBuilder

        // data-tid
        assertXPathBuilder("item", "//*[@data-tid=\"item\"]");
        assertXPathBuilder("*;item", "//*[@data-tid=\"item\"]");
        assertXPathBuilder("div;item", "//div[@data-tid=\"item\"]");
        assertXPathBuilder(";item", "[@data-tid=\"item\"]");

        // Special cases
        assertXPathBuilder("", "//*");
        assertXPathBuilder("*;", "//*");
        assertXPathBuilder("div;", "//div");
        assertXPathBuilder("ancestor::div;", "//ancestor::div");

        // With TAG
        assertXPathBuilder("div;id::", "//div[@id]");
        assertXPathBuilder("div;::item", "//div[@data-tid=\"item\"]");
        assertXPathBuilder("div;:-:item", "//div[contains(@data-tid,\"item\")]");
        assertXPathBuilder("div;id::item", "//div[@id=\"item\"]");
        assertXPathBuilder("div;id:-:item", "//div[contains(@id,\"item\")]");
        assertXPathBuilder("div;text()::item", "//div[.=\"item\"]");
        assertXPathBuilder("div;text():-:item", "//div[contains(text(),\"item\")]");

        // Without TAG - you can use it to append information to any XPath
        assertXPathBuilder(";id::", "[@id]");
        assertXPathBuilder(";::item", "[@data-tid=\"item\"]");
        assertXPathBuilder(";:-:item", "[contains(@data-tid,\"item\")]");
        assertXPathBuilder(";id::item", "[@id=\"item\"]");
        assertXPathBuilder(";id:-:item", "[contains(@id,\"item\")]");
        assertXPathBuilder(";text()::item", "[.=\"item\"]");
        assertXPathBuilder(";text():-:item", "[contains(text(),\"item\")]");
    }

   // @Test
    public void verifyPageObjectModelTest()
    {
        // TEST VARIABLES
        final String postcode = "EC2V 5DE";       // "EC2V 5DE"

        // Open a web page "hostURL"
        //singleDriver.get(siteLocalizedURL);
        singleDriver.get("http://neoworks.com");

        // ### Page Factory ###
        NeoHomePage neoHomePage = new NeoHomePage(singleDriver, this);

        // # STEP: Left click on "Contact Us" button from main menu.
        NeoContactUsPage neoContactUsPage = neoHomePage.clickMenuContactUs();

        // ### CHECKPOINT ###
        neoContactUsPage.verifyContactDetailsPostcode(postcode);
    }

    private void assertXPathBuilder(final String s, final String r)
    {
        assertTrue("\"" + s + "\" error", PageWrapper.neoXPathBuilder(s).equals(r));
    }
}