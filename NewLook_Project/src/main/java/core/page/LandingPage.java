package core.page;

import core.CucumberPageWrapper;
import core.TestWrapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by sreekanth.bongunuri on 19/11/15.
 */

@Component
public class LandingPage extends CucumberPageWrapper{

    @Autowired
    private TestWrapper testWrapper;


    By countrySelector = By.cssSelector("a[class='nl-select__choice ui-select-match']");
    By shopnowButton = By.cssSelector(".country__submit.button.button.button--secondary.button--secondary-black");
    By selectCountry = By.cssSelector(".ng-binding.ng-scope");

    public WebElement countrySelector(){
        return waitForExpectedElement(countrySelector);
    }

    public WebElement shopnowButton(){
        return waitForExpectedElement(shopnowButton);
    }

    public WebElement selectCountry(){
        return waitForExpectedElement(selectCountry);
    }


    public void selectCountry(String country) {

          List<WebElement> countryDropdown = driver().findElements(By.cssSelector(".ng-binding.ng-scope"));

          for (int i = 0; i < countryDropdown.size(); i++) {

              if (countryDropdown.get(i).getText().contentEquals(country)) {
                  driver().findElement(By.cssSelector("#ui-select-choices-row-0-" + --i + " > div.ui-select-choices-row-inner > div.ng-binding.ng-scope")).click();
                  break;
              }
          }
    }

    public String getCurrentUrl(){
        return driver().getCurrentUrl();
    }

}
