package core.page;

import core.CucumberPageWrapper;
import core.TestWrapper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class HomePage extends CucumberPageWrapper {

	@Autowired
	private TestWrapper testWrapper;


	By titleBrandLocator = By.cssSelector("a[title='Brands']");
	By streetWearLinkLocator = By.cssSelector("a[title='Streetwear']");
	By searchFieldLocator = By.id("js-site-search-input");
	By localeSelector = By.cssSelector("span.ng-scope");
	By changeSettingsLocaleButton = By.cssSelector("span[class*='locale-select__submit-button']");
	By storeLinkLocator = By.cssSelector("a[href='/uk/en/store-finder']");
	By countrySelector = By.cssSelector("div[ng-model='country.selected']");
	By currencySelector = By.cssSelector("div[ng-model='currency.selected']");
	By languageSelector = By.cssSelector("div[ng-model='language.selected']");
	By selectPdpProductImage = By.cssSelector("a[href*='/Backpacks/']");
	By productDetailsText = By.cssSelector(".tabhead>a");
	By selectedLocaleSettings = By.cssSelector(".link.link--nounderline.header__locale-select.locale-select__flag.locale-select__flag--gb");
	By countrySearchLocator = By.cssSelector("input[ng-model='$select.search']");
	By countryListSuggestions = By.cssSelector(".ui-select-choices-row-inner.nl-select__row-inner");




    public WebElement titleBrand(){
		return waitForExpectedElement(titleBrandLocator);
	}

	public WebElement streetWear(){
		return waitForExpectedElement(streetWearLinkLocator);
	}

	public WebElement searchFieldLocator(){
		return waitForExpectedElement(searchFieldLocator);
	}

	public WebElement localeSelector(){
		return waitForExpectedElement(localeSelector);
	}

	public WebElement changeSettingsLocaleButton(){
		return waitForExpectedElement(changeSettingsLocaleButton);
	}

	public WebElement storeLinkLocator(){
		return waitForExpectedElement(storeLinkLocator);
	}

	public WebElement countrySelector(){
		return waitForExpectedElement(countrySelector);
	}

	public WebElement currencySelector(){
		return waitForExpectedElement(currencySelector);
	}

	public WebElement languageSelector(){
		return waitForExpectedElement(languageSelector);
	}

	public WebElement selectPdpProductImage(){
		return waitForExpectedElement(selectPdpProductImage);
	}

	public WebElement productDetailsText(){
		return waitForExpectedElement(productDetailsText);
	}

	public WebElement selectedLocaleSettings(){
		return waitForExpectedElement(selectedLocaleSettings);
	}

	public WebElement countrySearchLocator(){
		return waitForExpectedElement(countrySearchLocator);
	}

	public WebElement countryListSuggestions(){
		return waitForExpectedElement(countryListSuggestions);
	}



	public List<String> returnPdpList(){
	    List<String> desctiprtion = new ArrayList<>();
			productDetailsText();
		List<WebElement> productDescription = driver().findElements(By.cssSelector(".tabhead>a"));
		for(int i=0;i<productDescription.size();i++){
			desctiprtion.add(productDescription.get(i).getText());
		}
	 return desctiprtion;
	}

	public void selectLanguage(String language){
		changeSettingsLocaleButton();
		(driver().findElement(By.cssSelector("div[ng-model='language.selected']"))).click();

		List<WebElement> languages = driver().findElements(By.cssSelector(".ui-select-choices-row-inner.nl-select__row-inner"));
		for(int i=0;i<languages.size();i++) {
			if (languages.get(i).getText().contains(language)) {
				WebElement selectLanguage = driver().findElement(By.id("ui-select-choices-row-2-" + i + ""));
				selectLanguage.click();
				break;
			}
		}
	}

	public void selectCountry(String country){
		changeSettingsLocaleButton();
		(driver().findElement(By.cssSelector("div[ng-model='country.selected']"))).click();

		List<WebElement> countries = driver().findElements(By.cssSelector(".ui-select-choices-row-inner.nl-select__row-inner"));
		for(int i=0;i<countries.size();i++){
			if(countries.get(i).getText().contains(country)){
				WebElement selectCountry = driver().findElement(By.id("ui-select-choices-row-0-"+i+""));
				selectCountry.click();
				((JavascriptExecutor) driver()).executeScript("window.scrollBy(0, -250)", "");
				break;
			}
		}

	}

	public void selectCurrency(String currency){
		(driver().findElement(By.cssSelector("div[ng-model='currency.selected']"))).click();
		List<WebElement> currencies = driver().findElements(By.cssSelector(".ui-select-choices-row-inner.nl-select__row-inner"));
		for(int i=0;i<currencies.size();i++) {
			if (currencies.get(i).getText().contains(currency)) {
				WebElement selectCurrency = driver().findElement(By.id("ui-select-choices-row-1-" + i + ""));
				selectCurrency.click();
				break;
			}
		}
	}

    public List<String> countrySuggestionsDisplayed(){
		countryListSuggestions();
		List<String> countries = new ArrayList<>();
		List<WebElement> countriesDisplayed = driver().findElements(By.cssSelector(".ui-select-choices-row-inner.nl-select__row-inner"));
		for(int i=0;i<countriesDisplayed.size();i++){
			countries.add(countriesDisplayed.get(i).getText());
		}
		return countries;

	}


	public String getCurrentUrl(){
		 return driver().getCurrentUrl();
	 }

	public List<String> getCountriesDisplayed(){
		countryListSuggestions();
		List<String> countries = new ArrayList<>();
		List<WebElement> countriesDisplayed = driver().findElements(By.cssSelector(".ui-select-choices-row-inner.nl-select__row-inner"));
		for(int i=0;i<countriesDisplayed.size();i++){
			countries.add(countriesDisplayed.get(i).getText());
		}
		return countries;
	}


	public boolean countryListDisplayed() {
		try {
			driver().findElement(By.cssSelector(".ui-select-choices-row-inner.nl-select__row-inner"));
			return true;
		} catch (org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}
	public String getCookies(){
		return driver().manage().getCookieNamed("newlookSessionData").toString();
	}

	public String noMatchesFound(){
		List<WebElement> noMathces = driver().findElements(By.cssSelector(".ng-binding.ng-scope"));
		return noMathces.get(1).getText();
	}

	public void hoverOverMegaBrand(String category){
		WebElement hoverElement = driver().findElement(By.cssSelector("a[title='"+category+"']"));
		Actions action = new Actions(driver());
		action.moveToElement(hoverElement).build().perform();

		hoverElement.click();

	}
}
