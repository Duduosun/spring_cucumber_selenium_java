package core.page;

import core.CucumberPageWrapper;
import core.TestWrapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by sreekanth.bongunuri on 06/11/15.
 */

@Component
public class LoginPage extends CucumberPageWrapper {


    @Autowired
    private TestWrapper testWrapper;


    By signinLinkLocator = By.cssSelector("a[href='/uk/en/login']");
    By registerLinkLocator = By.cssSelector("a[href='/uk/en/register']");
    By usernameFieldLocator = By.id("j_username");
    By passwordFieldLocator = By.id("j_password");
    By loginButtonLocator = By.cssSelector(".btn.btn-primary.btn-block");
    By loggedInUserLinkLocator = By.cssSelector("a[href='/uk/en/my-account']");
    By signoutLinkLocator = By.cssSelector("a[href='/uk/en/logout']");

    //Registration fields
    By title = By.id("register.title");
    By firstName = By.id("register.firstName");
    By lastName = By.id("register.lastName");
    By emailAddress = By.id("register.email");
    By password = By.id("password");
    By confirmPassword = By.id("register.checkPwd");
    By registerButton = By.cssSelector(".btn.btn-default.btn-block");





    public WebElement signinLinkLocator(){
        return waitForExpectedElement(signinLinkLocator);
    }

    public WebElement registerLinkLocator(){
        return waitForExpectedElement(registerLinkLocator);
    }

    public WebElement usernameFieldLocator(){
        return waitForExpectedElement(usernameFieldLocator);
    }

    public WebElement passwordFieldLocator(){
        return waitForExpectedElement(passwordFieldLocator);
    }

    public WebElement loginButtonLocator(){
        return waitForExpectedElement(loginButtonLocator);
    }

    public WebElement signoutLinkLocator(){
        return waitForExpectedElement(signoutLinkLocator);
    }

    public WebElement title(){
        return waitForExpectedElement(title);
    }

    public WebElement firstName(){
        return waitForExpectedElement(firstName);
    }

    public WebElement lastName(){
        return waitForExpectedElement(lastName);
    }

    public WebElement emailAddress(){
        return waitForExpectedElement(emailAddress);
    }

    public WebElement password(){
        return waitForExpectedElement(password);
    }

    public WebElement confirmPassword(){
        return waitForExpectedElement(confirmPassword);
    }

    public WebElement registerButton(){
        return waitForExpectedElement(registerButton);
    }

    public WebElement loggedInUserLinkLocator(){ return waitForExpectedElement(loggedInUserLinkLocator); }


    public void selectTitle(){
        title();
        new Select(driver().findElement(By.id("register.title"))).selectByVisibleText("Mr.");
    }


}
