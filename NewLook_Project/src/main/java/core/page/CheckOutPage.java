package core.page;

import core.CucumberPageWrapper;
import core.TestWrapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by sreekanth.bongunuri on 23/11/15.
 */
@Component
public class CheckOutPage extends CucumberPageWrapper {

    @Autowired
    private TestWrapper testWrapper;



    By addToCartButton = By.cssSelector("#addToCartButton");
    By checkOutLink = By.linkText("Checkout");
    By quantityInTheBasket = By.cssSelector(".qty");
    By basketIcon = By.cssSelector(".icon.icon--bag");
    By checkOutButton = By.cssSelector(".order-summary__checkout.button.checkoutButton");
    By countrySelector = By.id("address.country");
    By tileSelector = By.id("address.title");
    By firstname = By.id("address.firstName");
    By lastName = By.id("address.surname");
    By addressLine1 = By.id("address.line1");
    By city = By.id("address.townCity");
    By postCode = By.id("address.postcode");
    By paymentBillingAddressLink = By.cssSelector("a[href='/uk/en/checkout/multi/payment-method/add']");
    By selectCardType = By.name("card_type");
    By cardNumber = By.name("card_number");
    By monthOfExpiry = By.id("ExpiryMonth");
    By yearOfExpiry = By.id("ExpiryYear");
    By expiryDate = By.name("card_expiry_date");
    By cardVerificationNumber = By.name("card_cvn");
    By addressSubmitButton = By.id("addressSubmit");
    By deliverySubmitButton = By.id("deliveryMethodSubmit");
    By nextButton = By.name("submit");
    By useMyDeliveryAddress = By.id("useDeliveryAddress");
    By termsCheckBox = By.id("Terms1");
    By placeOrderButton = By.id("placeOrder");
    By checkouMessage = By.cssSelector(".checkout-success-headline");
    By guestEmailAddress = By.id("guest.email");
    By confirmGuestEmailAddress = By.id("guest.confirm.email");
    By checkOutAsGuestButton = By.cssSelector(".btn.btn-default.btn-block.guestCheckoutBtn");
    By bagId = By.cssSelector("p[class='order-summary__bag-id']");
    By priceSummary = By.cssSelector(".order-summary__value");
    By selectQuantity = By.id("quantitySelect");
    By indivudualPriceofTheItem = By.cssSelector("td[class='bag-item__col bag-item__price']");
    By basketCount_InTheIconBag = By.cssSelector(".header__bagcount");
    By priceItemBeforeQuantityIncrement = By.cssSelector(".bag-item__price--unit");
    By limitedQuantityProduct = By.cssSelector("a[href*='/Caps/']");
    By addToCartInput = By.id("pdpAddtoCartInput");


    public WebElement addToCartButton(){
        return waitForExpectedElement(addToCartButton);
    }

    public WebElement checkOutLink(){
        return waitForExpectedElement(checkOutLink);
    }

    public WebElement quantityInTheBasket(){
        return waitForExpectedElement(quantityInTheBasket);
    }

    public WebElement checkOutButton(){
        return waitForExpectedElement(checkOutButton);
    }

    public WebElement countrySelector(){
        return waitForExpectedElement(countrySelector);
    }

    public WebElement tileSelector(){
        return waitForExpectedElement(tileSelector);
    }

    public WebElement firstname(){
        return waitForExpectedElement(firstname);
    }

    public WebElement lastName(){
        return waitForExpectedElement(lastName);
    }

    public WebElement addressLine1(){
        return waitForExpectedElement(addressLine1);
    }

    public WebElement city(){
        return waitForExpectedElement(city);
    }

    public WebElement postCode(){
        return waitForExpectedElement(postCode);
    }

    public WebElement paymentBillingAddressLink(){
        return waitForExpectedElement(paymentBillingAddressLink);
    }

    public WebElement selectCardType(){
        return waitForExpectedElement(selectCardType);
    }

    public WebElement cardNumber(){
        return waitForExpectedElement(cardNumber);
    }

    public WebElement monthOfExpiry(){
        return waitForExpectedElement(monthOfExpiry);
    }

    public WebElement yearOfExpiry(){
        return waitForExpectedElement(yearOfExpiry);
    }

    public WebElement expiryDate(){
        return waitForExpectedElement(expiryDate);
    }

    public WebElement cardVerificationNumber(){
        return waitForExpectedElement(cardVerificationNumber);
    }

    public WebElement addressSubmitButton(){
        return waitForExpectedElement(addressSubmitButton);
    }

    public WebElement deliverySubmitButton(){
        return waitForExpectedElement(deliverySubmitButton);
    }

    public WebElement nextButton(){ return waitForExpectedElement(nextButton); }

    public WebElement useMyDeliveryAddress(){ return waitForExpectedElement(useMyDeliveryAddress); }

    public WebElement termsCheckBox(){ return waitForExpectedElement(termsCheckBox); }

    public WebElement placeOrderButton(){ return waitForExpectedElement(placeOrderButton); }

    public WebElement checkouMessage(){ return waitForExpectedElement(checkouMessage); }

    public WebElement guestEmailAddress(){ return waitForExpectedElement(guestEmailAddress); }

    public WebElement confirmGuestEmailAddress(){ return waitForExpectedElement(confirmGuestEmailAddress); }

    public WebElement checkOutAsGuestButton(){ return waitForExpectedElement(checkOutAsGuestButton); }

    public WebElement priceSummary(){ return waitForExpectedElement(priceSummary); }

    public WebElement bagId(){ return waitForExpectedElement(bagId); }

    public WebElement selectQuantity(){ return waitForExpectedElement(selectQuantity); }

    public WebElement basketIcon(){
        return waitForExpectedElement(basketIcon);
    }

    public WebElement indivudualPriceofTheItem(){
        return waitForExpectedElement(indivudualPriceofTheItem);
    }

    public WebElement addToCartInput(){
        return waitForExpectedElement(addToCartInput);
    }

    public WebElement limitedQuantityProduct(){
        return waitForExpectedElement(limitedQuantityProduct);
    }

    public WebElement basketCount_InTheIconBag(){
        return waitForExpectedElement(basketCount_InTheIconBag);
    }

    public WebElement priceItemBeforeQuantityIncrement(){
        return waitForExpectedElement(priceItemBeforeQuantityIncrement);
    }


    public void selectCountry(){
        countrySelector();
        new Select(driver().findElement(By.id("address.country"))).selectByVisibleText("United Kingdom");
    }

    public void selectTitle(){
        tileSelector();
        new Select(driver().findElement(By.id("address.title"))).selectByVisibleText("Mr.");
    }

    public void selectCard(){
       selectCardType();
        new Select(driver().findElement(By.name("card_type"))).selectByVisibleText("Visa");
    }

    public void selectExpiryMonth(){
        new Select(driver().findElement(By.id("ExpiryMonth"))).selectByVisibleText("06");
    }

    public void selectExpiryYear(){
        new Select(driver().findElement(By.id("ExpiryYear"))).selectByVisibleText("2016");
    }

    public void fillTheCheckoutAddressForm(){
        selectCountry();
        selectTitle();
        firstname().sendKeys("neo");
        lastName().sendKeys("works");
        addressLine1().sendKeys("Basinghall Street");
        city().sendKeys("London");
        postCode().sendKeys("EC2V 5DE");
    }

    public float getSubtotal(){
        String total = priceSummary().getText();
        String[] splitSubtotal = total.split("£");

        float subtotal = Float.parseFloat(splitSubtotal[1]);

        return subtotal;
    }

    public float getDeliveryTotal(){

        List<WebElement> cartPrices = driver().findElements(By.cssSelector(".order-summary__value"));
        String[] splitdeliveryTotal = cartPrices.get(2).getText().split("£");

        float deliveryTotal = Float.parseFloat(splitdeliveryTotal[1]);

        return deliveryTotal;
    }

    public String freeStandardDelivery(){
        List<WebElement> cartPrices = driver().findElements(By.cssSelector(".order-summary__value"));
        return cartPrices.get(1).getText();
    }


    public float getTotalPriceAfterQunatityIncrease(){
        indivudualPriceofTheItem();
         String[] splitPrice = indivudualPriceofTheItem().getText().trim().split(" ");
         String[] splitPriceAgain = splitPrice[0].substring(1,6).split(" ");
       return Float.parseFloat(splitPriceAgain[0]);

    }


    public float getpriceItemBeforeIncreasingThePrice(){
        indivudualPriceofTheItem();
        String[] splitPrice = priceItemBeforeQuantityIncrement().getText().trim().split("£");
        return Float.parseFloat(splitPrice[1].substring(0,5));
    }

    public void getStandardDeliveryTotal(){
        List<WebElement> cartPrices = driver().findElements(By.cssSelector(".order-summary__value"));
        if(cartPrices.get(1).getText().equals("FREE")){
             freeStandardDelivery();
        }

        else{
            ///need to implement this in the future when there is price avialable
        }
    }

    public void fillCardDetails(){
        selectCard();
        cardNumber().sendKeys("4444333322221111");
         expiryDate().sendKeys("04-2017");
        cardVerificationNumber().sendKeys("123");
    }

    public void selectQuantity(String quantity){
        selectQuantity();

        new Select(driver().findElement(By.id("quantitySelect"))).selectByVisibleText(quantity);
    }



}