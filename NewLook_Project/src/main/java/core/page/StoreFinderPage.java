package core.page;

import core.CucumberPageWrapper;
import core.TestWrapper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by sreekanth.bongunuri on 20/11/15.
 */
@Component
public class StoreFinderPage extends CucumberPageWrapper {

    @Autowired
    private TestWrapper testWrapper;

    By postCodeorTownFieldLocator = By.cssSelector("#storelocator-query");
    By postCodeSearchLocator = By.cssSelector("");

    public WebElement postCodeorTownFieldLocator(){
        return waitForExpectedElement(postCodeorTownFieldLocator);
    }

}
