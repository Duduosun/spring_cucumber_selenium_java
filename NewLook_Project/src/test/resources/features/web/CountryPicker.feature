 @CountryPicker
  Feature: As a Customer
    I want to be able to search for my delivery country in the Country picker,
    so that I can find my country quicker than scrolling through the list

  @NL-14
  Scenario Outline: Country suggestions displayed up on typing in the country selector search field
    Given i navigate to "Newlook" home page
    When I have opened the Country picker
    And I have typed a minimum of one "<character>" in to the search field
    And a refined selection of "<countries>" is presented to me

    Examples:
    |character|countries|
    |united   |united   |
    |est      |est      |
    |a        |a        |

    @NL-14
    Scenario:
      Given i navigate to "Newlook" home page
      And I have not entered any text in to the search field
      Then I can see an un-refined list of countries

      @NL-14
      Scenario: No countries found for wrong search in country picker
        Given i navigate to "Newlook" home page
        When I have opened the Country picker
        And I have typed some random character in to the search field
        When no countries can be matched ,I'm notified that there are no mathces
        Then I'm notified that there are no mathces as no countries were matched

