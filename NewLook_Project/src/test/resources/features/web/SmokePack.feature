
Feature: New look smoke pack feature

  Scenario: Customer getting redricted to the landing page as the geo location cannot be found using his IP address
    Given I am an unrecognised customer whose IP address cannot be located using geo location
    When I access the new look site
    Then I am directed to the landing page


  Scenario Outline: Locale select on landing page
    Given I am on the Landing Page
    When I select default "<country>" from the list
    And I am directed to the homepage for the store that supports the "<countrychosen>"
    Then the cookie should be set for "<language>","<currency>","<countryId>" selection
    Examples:
      |     country       |countrychosen    |language|currency|countryId|
      |   United Kingdom  |uk/en?country=GB |  en    |  GBP   |    uk   |


  Scenario Outline: Locale select on landing page
    Given I am on the Landing Page
    When I expand the countries list using the picker
    And I select a "<country>" from the list
    And I am directed to the homepage for the store that supports the "<countrychosen>"
    Then the cookie should be set for "<language>","<currency>","<countryId>" selection

    Examples:
      |     country       |countrychosen    |language|currency|countryId|
      |      France       | fr?country=FR   |  fr    |  EUR   |   fr    |
      |      Denmark      |en?country=DK    |  en    |  EUR   |  de     |
      |    United States  |row/en?country=US|  en    |  EUR   |  row    |



  Scenario Outline: Log in
      Given i navigate to "Newlook" home page
      When I click Sign In on the header bar
      And I enter valid login details "<username>" and "<password>" in the log in form
      And I submit the form
      Then I am logged in
      And I can see my user name displayed in the header in the form "<name>" account
      Then check the currency selected in the locale selector is "<changedCurrency>"

      Examples:
        |     username         |password  |    name   |changedCurrency|
        | jrosenberg@salmon.com|Password01| judith's  |EUR (€)        |


     Scenario: Register
       Given i navigate to "Newlook" home page
       When I click Sign In on the header bar
      And I enter valid login details into the registration form
       And I submit the registration form
       Then I am logged in


      Scenario: Log out
        Given i navigate to "Newlook" home page
        And I am a logged in user on the New Look site
        When I click on account header
        And click on signout
        Then I should be logged out from the account


       Scenario Outline: Search Results
          Given i navigate to "Newlook" home page
          When I enter a valid "<search>" term into the search bar
          And I click enter OR click on the search icon
          Then I am directed to the search results page
          And the results are relevant for the search term entered

          Examples:
          |search|
          |shoes |


        Scenario Outline: Change language in header
          Given i navigate to "Newlook" home page
          When I click on the locale selector in the header
          And I select a different country "<country>" using the drop down
          And I select a different currency "<currency>" using the drop down
          And I select a different language "<language>" using the drop down
          Then the page will be reloaded
         # And I will remain in the same store
          And the picker should contain "<languageInTheLocaleSelector>"
          And when I click on the locale selector the language"<selectedLanguage>" that was chosen will be displayed in the picker
          And the url chould contain "<selectedlanguage>"

          Examples:
          |country       | currency  |  language    |selectedLanguage|selectedlanguage|languageInTheLocaleSelector |
          |France        |  EUR (€)  |  French      |  Français      | /fr/fr?        |France - € - Français       |
          |United States |  USD ($)  |  English     |  English       | /row/en?       |United States - $ - English |


  Scenario Outline: Change country in header
    Given i navigate to "Newlook" home page
    When I click on the locale selector in the header
    And I select a different country "<country>" using the drop down
    Then the page will be reloaded
    #And I will remain in the same store
    And when I click on the locale selector the country "<selectedCountry>" that was chosen will be displayed in the picker
    And the url chould contain "<countryChosen>"

    Examples:
      |  country   |selectedCountry|countryChosen|
      |  Ireland    |  Ireland     | &country=IE |

  Scenario Outline: Change currency in header
    Given i navigate to "Newlook" home page
    When I click on the locale selector in the header
    And I select a different country "<country>" using the drop down
    And I select a different currency "<currency>" using the drop down
    Then the page will be reloaded
    #And I will remain in the same store
    And when I click on the locale selector the currency "<selectedCurrency>" that was chosen will be displayed in the picker
    And the url chould contain "<currencyChosen>"

    Examples:
      |country|  currency   |selectedCurrency |currencyChosen|
      |Germany|  EUR (€)    |  EUR (€)        |currency=EUR  |


Scenario Outline: Find Store
  Given i navigate to "Newlook" home page
  When navigated to the Store Finder page
  And enter a valid "<postcode_or_town>" into the field
  And I click Search
  Then the store location results are displayed
  And the results relate to the "<expectedSearch>" that I entered

  Examples:
  |postcode_or_town|expectedSearch|
  | W1J 5RJ        |   W1J 5RJ    |


  Scenario Outline: PLP
    Given i navigate to "Newlook" home page
    When I click on a "<category>" using the megamenu navigation
    Then I am directed to a PLP displaying "<products>" relating to the category selected
    Examples:
    |category  |products        |
    |Brands    |brands          |
    |Streetwear|streetwear      |


 Scenario: Navigate to PDP
   Given i navigate to "Newlook" home page
    #When I navigated to a PLP on the New Look site
    And click on a product image or title
    Then I am directed to the PDP for that product


  Scenario:Add item to basket
    Given i navigate to "Newlook" home page
    When click on a product image or title
    And I click Add to Bag
    Then the product is added to my basket
    And the mini-bag is displayed with my item displayed

  Scenario:Add item to basket and checkout as logged in user
    Given i navigate to "Newlook" home page
    And I am a logged in user on the New Look site
    When click on a product image or title
    And I click Add to Bag
    Then the product is added to my basket
    When i click on checkout button
    And fill the shipping details
    And fill the card details
    And click checkout
    Then you should see acheckout message

  @smokeTests
  Scenario:Add item to basket and checkout as a guest user
    Given i navigate to "Newlook" home page
    When click on a product image or title
    And I click Add to Bag
    Then the product is added to my basket
    When i click on checkout button
    And continue as guest
    And fill the shipping details
    And fill the card details
    And click checkout
    Then you should see acheckout message