package testJourney;

import core.CucumberPageWrapper;
import core.TestWrapper;
import core.page.*;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import utils.RandomGenerator;

/**
 * Created by sreekanth.bongunuri on 19/11/15.
 */

@ContextConfiguration("classpath:cucumber.xml")
public class SmokePackStepDefinitions {

    @Autowired
    private TestWrapper testWrapper;

    @Autowired
    private CucumberPageWrapper cucumberPageWrapper;

    @Autowired
    private LandingPage landingPage;

    @Autowired
    private LoginPage loginPage;

    @Autowired
     private HomePage homePage;

    @Autowired
     private StoreFinderPage storeFinderPage;

    @Autowired
    private CheckOutPage checkOutPage;

    RandomGenerator randomGenerator;

    String generatedName;


    @Given("^I am an unrecognised customer whose IP address cannot be located using geo location$")
    public void I_am_an_unrecognised_customer_whose_IP_address_cannot_be_located_using_geo_location() {
        testWrapper.singleDriver.manage().deleteAllCookies();
        testWrapper.singleDriver.navigate().refresh();

    }

    @When("^I access the new look site$")
    public void I_access_the_new_look_site() throws Throwable {
        testWrapper.singleDriver.get("http://qa01.nlk374.neoworks.co.uk");

    }

    @Then("^I am directed to the landing page$")
    public void I_am_directed_to_the_landing_page() throws Throwable {
        Assert.assertTrue(testWrapper.singleDriver.getCurrentUrl().contains("landing"));
    }


    @When("^I expand the countries list using the picker$")
    public void I_expand_the_countries_list_using_the_picker() throws Throwable {
       landingPage.countrySelector().click();
    }

    @When("^I select default \"([^\"]*)\" from the list$")
    public void I_select_default_from_the_list(String country) throws Throwable {
      landingPage.shopnowButton().click();
    }

    @And("^I select a \"([^\"]*)\" from the list$")
    public void I_select_a_from_the_list(String country) throws InterruptedException {
        landingPage.selectCountry(country);
        landingPage.shopnowButton().click();

    }


    @Then("^I am directed to the homepage for the store that supports the \"([^\"]*)\"$")
    public void I_am_directed_to_the_homepage_for_the_store_that_supports_the(String selectedCountry) {
       Assert.assertTrue(landingPage.getCurrentUrl().contains(selectedCountry));

        System.out.println("set cookies--->"+homePage.getCookies());
    }
    @Then("^the cookie should be set for \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\" selection$")
    public void the_cookie_should_be_set_for_selection(String language, String currency, String countryId)  {
        Assert.assertTrue(homePage.getCookies().contains(language));
        Assert.assertTrue(homePage.getCookies().contains(currency));
        Assert.assertTrue(homePage.getCookies().contains(countryId));

        //Delete the cookies which were set
        homePage.driver().manage().deleteCookieNamed("newlookSessionData");
        homePage.driver().manage().deleteCookieNamed("JSESSIONID");

    }


    @When("^I click Sign In on the header bar$")
    public void I_click_Sign_In_on_the_header_bar()  {
         loginPage.signinLinkLocator().click();
    }

    @And("^I enter valid login details \"([^\"]*)\" and \"([^\"]*)\" in the log in form$")
    public void I_enter_valid_login_details_and_in_the_log_in_form(String username, String password)  {
        loginPage.usernameFieldLocator().sendKeys(username);
        loginPage.passwordFieldLocator().sendKeys(password);
    }

    @And("^I submit the form$")
    public void I_submit_the_form() throws Throwable {
       loginPage.loginButtonLocator().click();
    }

    @Then("^I am logged in$")
    public void I_am_logged_in() throws Throwable {
        Assert.assertTrue(loginPage.loggedInUserLinkLocator().isDisplayed());
    }

    @When("^I click on account header$")
    public void I_click_on_account_header() throws Throwable {
       loginPage.loggedInUserLinkLocator().click();
    }

    @And("^click on signout$")
    public void click_on_signout() throws Throwable {
      loginPage.signoutLinkLocator().click();
    }

    @Then("^I should be logged out from the account$")
    public void I_should_be_logged_out_from_the_account() throws Throwable {
        Assert.assertTrue(loginPage.signinLinkLocator().isDisplayed());
    }


    @And("^I can see my user name displayed in the header in the form \"([^\"]*)\" account$")
    public void I_can_see_my_user_name_displayed_in_the_header_in_the_form_account(String name)  {
        Assert.assertTrue(loginPage.loggedInUserLinkLocator().getText().contains(name));
    }

    @Then("^check the currency selected in the locale selector is \"([^\"]*)\"$")
    public void check_the_currency_selected_in_the_locale_selector_is(String currency) throws Throwable {
        homePage.localeSelector().click();
        Assert.assertTrue(homePage.currencySelector().getText().contains(currency));
    }

    @And("^I enter valid login details into the registration form$")
    public void I_enter_valid_login_details_into_the_registration_form() {
        loginPage.selectTitle();
        this.generatedName = randomGenerator.randomAlphabetic(5);
        loginPage.firstName().sendKeys( this.generatedName);
        loginPage.lastName().sendKeys(randomGenerator.randomAlphabetic(5));
        loginPage.emailAddress().sendKeys(randomGenerator.randomEmailAddress(5));
        loginPage.password().sendKeys("Pa55word");
        loginPage.confirmPassword().sendKeys("Pa55word");
    }

    @And("^I submit the registration form$")
    public void I_submit_the_registration_form(){
          loginPage.registerButton().click();
    }


    @And("^I can see my user name displayed in the header form of the account$")
    public void I_can_see_my_user_name_displayed_in_the_header_form_of_the_account(){
        Assert.assertTrue(loginPage.loggedInUserLinkLocator().getText().contains(this.generatedName));
    }

    @Given("^I am a logged in user on the New Look site$")
    public void I_am_a_logged_in_user_on_the_New_Look_site(){
         loginPage.signinLinkLocator().click();
        loginPage.usernameFieldLocator().sendKeys("jrosenberg@salmon.com");
        loginPage.passwordFieldLocator().sendKeys("Password01");
        loginPage.loginButtonLocator().click();
    }

    @When("^I hover over account name in the header$")
    public void I_hover_over_account_name_in_the_header() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @And("^I click the Sign out option$")
    public void I_click_the_Sign_out_option() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }


    @Then("^I can see the Sign ,register link in the header$")
    public void I_can_see_the_Sign_register_link_in_the_header()  {
        Assert.assertTrue(loginPage.signinLinkLocator().isDisplayed());
        Assert.assertTrue(loginPage.registerLinkLocator().isDisplayed());
    }

    @When("^I enter a valid \"([^\"]*)\" term into the search bar$")
    public void I_enter_a_valid_term_into_the_search_bar(String searchWord) {
         homePage.searchFieldLocator().sendKeys(searchWord);
    }

    @And("^I click enter OR click on the search icon$")
    public void I_click_enter_OR_click_on_the_search_icon() {
        homePage.searchFieldLocator().submit();
    }

    @And("^I select a different language \"([^\"]*)\" using the drop down$")
    public void I_select_a_different_language_using_the_drop_down(String language)  {
      homePage.selectLanguage(language);
    }

    @And("^I select a different country \"([^\"]*)\" using the drop down$")
    public void I_select_a_different_country_using_the_drop_down(String country) {
        homePage.selectCountry(country);
    }

    @And("^I select a different currency \"([^\"]*)\" using the drop down$")
    public void I_select_a_different_currency_using_the_drop_down(String currency)  {
       homePage.selectCurrency(currency);
    }

    @Then("^I am directed to the search results page$")
    public void I_am_directed_to_the_search_results_page(){
       Assert.assertTrue(homePage.getCurrentUrl().contains("search/"));
    }

    @And("^the results are relevant for the search term entered$")
    public void the_results_are_relevant_for_the_search_term_entered() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @When("^I click on the locale selector in the header$")
    public void I_click_on_the_locale_selector_in_the_header()  {
      homePage.localeSelector().click();
    }


    @Then("^the page will be reloaded$")
    public void the_page_will_be_reloaded() throws Throwable {
       homePage.changeSettingsLocaleButton().click();
    }

    @And("^I will remain in the same store$")
    public void I_will_remain_in_the_same_store() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }


    @And("^when I click on the locale selector the country \"([^\"]*)\" that was chosen will be displayed in the picker$")
    public void when_I_click_on_the_locale_selector_the_country_that_was_chosen_will_be_displayed_in_the_picker(String selectedCountry) throws Throwable {
        homePage.localeSelector().click();
        Assert.assertTrue(homePage.countrySelector().getText().contains(selectedCountry));
    }

    @And("^when I click on the locale selector the language\"([^\"]*)\" that was chosen will be displayed in the picker$")
    public void when_I_click_on_the_locale_selector_the_language_that_was_chosen_will_be_displayed_in_the_picker(String selectedLanguage) throws Throwable {
        homePage.localeSelector().click();
        Assert.assertTrue(homePage.languageSelector().getText().contains(selectedLanguage));
    }

    @And("^when I click on the locale selector the currency \"([^\"]*)\" that was chosen will be displayed in the picker$")
    public void when_I_click_on_the_locale_selector_the_currency_that_was_chosen_will_be_displayed_in_the_picker(String selectedCurrency) throws Throwable {
        homePage.localeSelector().click();
        Assert.assertTrue(homePage.currencySelector().getText().contains(selectedCurrency));
    }

    @And("^the url chould contain \"([^\"]*)\"$")
    public void the_url_chould_contain(String countryChosen) throws Throwable {
      homePage.getCurrentUrl().contains(countryChosen);
    }

    @And("^the picker should contain \"([^\"]*)\"$")
    public void the_picker_should_contain(String pickerLanguage) {

        System.out.println("The text" + homePage.localeSelector().getText());
        Assert.assertTrue(homePage.localeSelector().getText().contains(pickerLanguage));
    }

    @When("^navigated to the Store Finder page$")
    public void navigated_to_the_Store_Finder_page() {
       homePage.storeLinkLocator().click();
    }

    @And("^enter a valid \"([^\"]*)\" into the field$")
    public void enter_a_valid_into_the_field(String postCode)  {
       storeFinderPage.postCodeorTownFieldLocator().sendKeys(postCode);
    }

    @And("^I click Search$")
    public void I_click_Search() throws Throwable {
        storeFinderPage.postCodeorTownFieldLocator().submit();
    }

    @Then("^the store location results are displayed$")
    public void the_store_location_results_are_displayed() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @And("^the results relate to the \"([^\"]*)\" that I entered$")
    public void the_results_relate_to_the_that_I_entered(String arg1) throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @When("^I click on a \"([^\"]*)\" using the megamenu navigation$")
    public void I_click_on_a_using_the_megamenu_navigation(String category) throws InterruptedException {
        homePage.hoverOverMegaBrand(category);

    }



    @Then("^I am directed to a PLP displaying \"([^\"]*)\" relating to the category selected$")
    public void I_am_directed_to_a_PLP_displaying_relating_to_the_category_selected(String selectedPlp) throws Throwable {
        homePage.getCurrentUrl().contains(selectedPlp);
    }

    @And("^click on a product image or title$")
    public void click_on_a_product_image_or_title() throws Throwable {
         homePage.selectPdpProductImage().click();
    }

    @Then("^I am directed to the PDP for that product$")
    public void I_am_directed_to_the_PDP_for_that_product() throws Throwable {
       Assert.assertTrue(homePage.returnPdpList().contains("Product details"));
        Assert.assertTrue(homePage.returnPdpList().contains("Specs"));
        Assert.assertTrue(homePage.returnPdpList().contains("Shipping"));

    }

    @And("^I click Add to Bag$")
    public void I_click_Add_to_Bag()  {
       checkOutPage.addToCartButton().click();

    }

    @Then("^the product is added to my basket$")
    public void the_product_is_added_to_my_basket() throws Throwable {
        checkOutPage.checkOutLink().click();
    }

    @And("^the mini-bag is displayed with my item displayed$")
    public void the_mini_bag_is_displayed_with_my_item_displayed() {
       checkOutPage.basketIcon().click();
        Assert.assertTrue(checkOutPage.quantityInTheBasket().getText().contains("Quantity: 1"));
    }


    @When("^i click on checkout button$")
    public void i_click_on_checkout_button(){
     checkOutPage.checkOutButton().click();
    }


    @And("^fill the shipping details$")
    public void fill_the_shipping_details() throws Throwable {
      checkOutPage.fillTheCheckoutAddressForm();
        checkOutPage.addressSubmitButton().click();
        checkOutPage.paymentBillingAddressLink().click();
    }

    @And("^fill the card details$")
    public void fill_the_card_details() throws Throwable {
        checkOutPage.fillCardDetails();
    }

    @And("^click checkout$")
    public void click_checkout() throws Throwable {
       checkOutPage.nextButton().click();
        checkOutPage.termsCheckBox().click();
        checkOutPage.placeOrderButton().click();
    }

    @Then("^you should see acheckout message$")
    public void you_should_see_acheckout_message() throws Throwable {
        Assert.assertTrue(checkOutPage.checkouMessage().getText().contains("Checkout Successful"));
    }

    @And("^continue as guest$")
    public void continue_as_guest() throws Throwable {
       checkOutPage.guestEmailAddress().sendKeys("srbongunuri@salmon.com");
        checkOutPage.confirmGuestEmailAddress().sendKeys("srbongunuri@salmon.com");
        checkOutPage.checkOutAsGuestButton().click();
    }

    @cucumber.api.java.After
    public void tearDown(Scenario scenario) {

        if (scenario.isFailed()) {
            byte[] screenshot = ((TakesScreenshot) testWrapper.singleDriver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        }
    }


}
