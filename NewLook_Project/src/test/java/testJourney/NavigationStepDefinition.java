package testJourney;

import core.CucumberPageWrapper;
import core.TestWrapper;
import cucumber.api.java.en.Given;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by sreekanth.bongunuri on 06/11/15.
 */
public class NavigationStepDefinition {

    @Autowired
    private TestWrapper testWrapper;

    @Autowired
    private CucumberPageWrapper cucumberPageWrapper;

    @Given("^i navigate to \"(.*?)\" home page$")
    public void i_navigate_to_the_page(String pageName) throws Throwable {

      testWrapper.singleDriver.manage().deleteAllCookies();
        testWrapper.singleDriver.navigate().refresh();
      testWrapper.singleDriver.get(testWrapper.siteLocalizedURL);
    }

    @Given("^I am on the Landing Page$")
    public void I_am_on_the_Landing_Page()  {

        //Delete cookies if they still exists forcefully
        testWrapper.singleDriver.manage().deleteCookieNamed("JSESSIONID");
        testWrapper.singleDriver.manage().deleteCookieNamed("newlookSessionData");

        //Then navigate to the URL
        testWrapper.singleDriver.get("http://qa01.nlk374.neoworks.co.uk/landing/en");

    }
}
