package testJourney;

import core.CucumberPageWrapper;
import core.TestWrapper;
import core.page.CheckOutPage;
import core.page.HomePage;
import core.page.LoginPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by sreekanth.bongunuri on 07/12/15.
 */

@ContextConfiguration("classpath:cucumber.xml")
public class ShoppingCartStepDefinitions {

    @Autowired
    private TestWrapper testWrapper;

    @Autowired
    private CucumberPageWrapper cucumberPageWrapper;

    @Autowired
    HomePage homePage;

    @Autowired
    LoginPage loginPage;

    @Autowired
    CheckOutPage checkOutPage;


    @Then("^I am shown a breakdown of costs to be applied to my purchase$")
    public void I_am_shown_a_breakdown_of_costs_to_be_applied_to_my_purchase() {
     double standardDeliverTotal = 0;

     if(checkOutPage.freeStandardDelivery().equals("FREE"))
     {
        standardDeliverTotal = 0.00;
     }

        Assert.assertTrue(checkOutPage.getSubtotal()+standardDeliverTotal==checkOutPage.getDeliveryTotal());

      System.out.println(checkOutPage.getSubtotal());
        System.out.println(checkOutPage.freeStandardDelivery());
        System.out.println(checkOutPage.getDeliveryTotal());
    }

    @Then("^I can see a cart ID displayed$")
    public void I_can_see_a_cart_ID_displayed(){
        Assert.assertTrue(checkOutPage.bagId().getText().matches("Bag ID: "+"\\d{8}"));
    }

    @And("^I view my cart Quick View side panel$")
    public void I_view_my_cart_Quick_View_side_panel() throws Throwable {
        throw new PendingException();
    }

    @Then("^I CANNOT see a cart ID displayed$")
    public void I_CANNOT_see_a_cart_ID_displayed() {
        throw new PendingException();
    }

    @When("^I view my cart, as Quick View or full view$")
    public void I_view_my_cart_as_Quick_View_or_full_view() throws Throwable {
        throw new PendingException();
    }

    @And("^I have qualified for a promotion$")
    public void I_have_qualified_for_a_promotion() throws Throwable {
        throw new PendingException();
    }

    @When("^I review the quick view cart$")
    public void I_review_the_quick_view_cart() throws Throwable {

        throw new PendingException();
    }

    @Then("^I am shown an order subtotal$")
    public void I_am_shown_an_order_subtotal() throws Throwable {

        throw new PendingException();
    }

    @And("^the subtotal does not include any delivery costs$")
    public void the_subtotal_does_not_include_any_delivery_costs() throws Throwable {
        throw new PendingException();
    }


    @And("^I increase the \"([^\"]*)\" of a line item$")
    public void I_increase_the_of_a_line_item(String quantity)  {
        checkOutPage.selectQuantity(quantity);
    }

    @Then("^the additional products will be committed to my cart with the same \"([^\"]*)\"$")
    public void the_additional_products_will_be_committed_to_my_cart_with_the_same(String quantity){

        float actualCartPriceAfterIncrement = checkOutPage.getpriceItemBeforeIncreasingThePrice();
         Assert.assertTrue(checkOutPage.basketCount_InTheIconBag().getText()==quantity);
        Assert.assertTrue(actualCartPriceAfterIncrement*Integer.parseInt(quantity)==checkOutPage.getTotalPriceAfterQunatityIncrease());
    }


    @When("^click on a product with limited stock$")
    public void click_on_a_product_with_limited_stock()  {
      checkOutPage.limitedQuantityProduct().click();
    }

    @Then("^increase the product quantity before adding to the bag$")
    public void increase_the_prodcut_quantity_before_adding_to_the_bag() {
       checkOutPage.addToCartInput().clear();
        checkOutPage.addToCartInput().sendKeys("4");
    }

    @Then("^I am only shown the quantity available in the picker$")
    public void I_am_only_shown_the_quantity_available_in_the_picker() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }


    @And("^I decrease the quantity of the line item$")
    public void I_decrease_the_quantity_of_the_line_item() throws Throwable {
         checkOutPage.selectQuantity("3");
    }

    @Then("^the unwanted product/s will be removed from my cart$")
    public void the_unwanted_product_s_will_be_removed_from_my_cart() throws Throwable {

    }
}
