package testJourney;

import core.CucumberPageWrapper;
import core.TestWrapper;
import core.page.HomePage;
import core.page.LoginPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;


@ContextConfiguration("classpath:cucumber.xml")
public class HomePageStepDefinitions {

	@Autowired
	private TestWrapper testWrapper;

	@Autowired
	private CucumberPageWrapper cucumberPageWrapper;

	@Autowired
	HomePage homePage;

	@Autowired
	LoginPage loginPage;



	@When("^i click on root category link$")
	public void i_click_on_root_category_link(){

		 homePage.titleBrand().click();
		//homePage.clickTitleBrand();
		Assert.assertTrue(homePage.streetWear().isDisplayed());

	}

	@When("^i click on sign in link$")
	public void i_click_on_sign_in_link() throws InterruptedException {

		loginPage.signinLinkLocator().click();
	}

	@And("^enter \"([^\"]*)\" ,\"([^\"]*)\"$")
	public void enter_(String username, String password) {

		loginPage.usernameFieldLocator().sendKeys(username);
		loginPage.passwordFieldLocator().sendKeys(password);
	}

	@And("^click enter$")
	public void click_enter() {

		loginPage.loginButtonLocator().click();
	}

	@Then("^i should be logged in$")
	public void i_should_be_logged_in() {

		Assert.assertTrue(loginPage.loggedInUserLinkLocator().isDisplayed());

	}



}
