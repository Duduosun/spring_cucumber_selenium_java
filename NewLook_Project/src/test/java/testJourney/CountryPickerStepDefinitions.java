package testJourney;

import core.CucumberPageWrapper;
import core.TestWrapper;
import core.page.HomePage;
import core.page.LoginPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sreekanth.bongunuri on 04/12/15.
 */

@ContextConfiguration("classpath:cucumber.xml")
public class CountryPickerStepDefinitions {

    @Autowired
    private TestWrapper testWrapper;

    @Autowired
    private CucumberPageWrapper cucumberPageWrapper;

    @Autowired
    HomePage homePage;

    @Autowired
    LoginPage loginPage;



    @When("^I have opened the Country picker$")
    public void I_have_opened_the_Country_picker()  {
       homePage.localeSelector().click();
        homePage.countrySelector().click();

    }

    @And("^I have typed a minimum of one \"([^\"]*)\" in to the search field$")
    public void I_have_typed_a_minimum_of_one_in_to_the_search_field(String searchWord)  {
        homePage.countrySearchLocator().sendKeys(searchWord);
    }


    @And("^a refined selection of \"([^\"]*)\" is presented to me$")
    public void a_refined_selection_of_is_presented_to_me(String suggestionsDisplayed) {
        List<String> countriesSuggested = new ArrayList<>();
        countriesSuggested = homePage.countrySuggestionsDisplayed();
        for(int i=0; i<countriesSuggested.size();i++){
            Assert.assertTrue(countriesSuggested.get(i).toLowerCase().contains(suggestionsDisplayed));
        }
    }

    @And("^I have not entered any text in to the search field$")
    public void I_have_not_entered_any_text_in_to_the_search_field(){
        homePage.localeSelector().click();
        homePage.countrySelector().click();
        homePage.countrySearchLocator().clear();
    }

    @Then("^I can see an un-refined list of countries$")
    public void I_can_see_an_un_refined_list_of_countries() {
        List<String> countriesSuggested = new ArrayList<>();
        countriesSuggested = homePage.getCountriesDisplayed();
        Assert.assertTrue(countriesSuggested.size()==259);
    }

    @And("^I have typed some random character in to the search field$")
    public void I_have_typed_some_random_character_in_to_the_search_field() {
        homePage.countrySearchLocator().sendKeys("abcde");
    }

    @When("^no countries can be matched ,I'm notified that there are no mathces$")
    public void no_countries_can_be_matched_I_m_notified_that_there_are_no_mathces() {
           Assert.assertTrue(homePage.countryListDisplayed()==false);
    }

    @Then("^I'm notified that there are no mathces as no countries were matched$")
    public void I_m_notified_that_there_are_no_mathces_as_no_countries_were_matched() {
         Assert.assertEquals("No matches found" , homePage.noMatchesFound());
    }


}
