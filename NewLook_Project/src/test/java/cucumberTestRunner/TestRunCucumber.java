package cucumberTestRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


/**
 * Created by sreekanth.bongunuri on 05/11/15.
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/web",tags={"@guii"},monochrome = true, plugin = {
        "pretty", "html:target/cucumber-report/",
        "json:target/cucumber-report/cucumber.json"},glue="testJourney")


public class TestRunCucumber {

}

