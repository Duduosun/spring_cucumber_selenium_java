$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ShoppingCart.feature");
formatter.feature({
  "line": 2,
  "name": "Shopping cart feature",
  "description": "",
  "id": "shopping-cart-feature",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@ShoppingCart"
    },
    {
      "line": 1,
      "name": "@checkout"
    }
  ]
});
formatter.scenario({
  "line": 77,
  "name": "Decrease the quantity of the item cart",
  "description": "",
  "id": "shopping-cart-feature;decrease-the-quantity-of-the-item-cart",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 76,
      "name": "@guii"
    },
    {
      "line": 76,
      "name": "@NL-228"
    }
  ]
});
formatter.step({
  "line": 78,
  "name": "i navigate to \"Newlook\" home page",
  "keyword": "Given "
});
formatter.step({
  "line": 79,
  "name": "click on a product image or title",
  "keyword": "When "
});
formatter.step({
  "line": 80,
  "name": "increase the product quantity before adding to the bag",
  "keyword": "Then "
});
formatter.step({
  "line": 81,
  "name": "I click Add to Bag",
  "keyword": "And "
});
formatter.step({
  "line": 82,
  "name": "the product is added to my basket",
  "keyword": "Then "
});
formatter.step({
  "line": 83,
  "name": "I decrease the quantity of the line item",
  "keyword": "And "
});
formatter.step({
  "line": 84,
  "name": "the unwanted product/s will be removed from my cart",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Newlook",
      "offset": 15
    }
  ],
  "location": "NavigationStepDefinition.i_navigate_to_the_page(String)"
});
formatter.result({
  "duration": 1291012725,
  "status": "passed"
});
formatter.match({
  "location": "SmokePackStepDefinitions.click_on_a_product_image_or_title()"
});
formatter.result({
  "duration": 647620232,
  "status": "passed"
});
formatter.match({
  "location": "ShoppingCartStepDefinitions.increase_the_prodcut_quantity_before_adding_to_the_bag()"
});
formatter.result({
  "duration": 1463534869,
  "status": "passed"
});
formatter.match({
  "location": "SmokePackStepDefinitions.I_click_Add_to_Bag()"
});
formatter.result({
  "duration": 556832234,
  "status": "passed"
});
formatter.match({
  "location": "SmokePackStepDefinitions.the_product_is_added_to_my_basket()"
});
formatter.result({
  "duration": 624332959,
  "status": "passed"
});
formatter.match({
  "location": "ShoppingCartStepDefinitions.I_decrease_the_quantity_of_the_line_item()"
});
formatter.result({
  "duration": 801662473,
  "status": "passed"
});
formatter.match({
  "location": "ShoppingCartStepDefinitions.the_unwanted_product_s_will_be_removed_from_my_cart()"
});
formatter.result({
  "duration": 42794,
  "status": "passed"
});
formatter.after({
  "duration": 108062,
  "status": "passed"
});
});