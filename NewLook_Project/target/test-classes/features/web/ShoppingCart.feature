@ShoppingCart @checkout
Feature: Shopping cart feature

  @NL-234
  Scenario: costs break down
    Given i navigate to "Newlook" home page
    When click on a product image or title
    And I click Add to Bag
    Then the product is added to my basket
    Then I am shown a breakdown of costs to be applied to my purchase

  @NL-234
    Scenario: Shopping cart using promotional code
      Given i navigate to "Newlook" home page
      When click on a product image or title
      And I click Add to Bag
      Then the product is added to my basket
      And I have qualified for a promotion
      Then I am shown a breakdown of costs to be applied to my purchase

      @NL-234
      Scenario: Cart quick view doesn't have any included delivery costs
        Given i navigate to "Newlook" home page
        When click on a product image or title
        And I click Add to Bag
        Then the product is added to my basket
        When I review the quick view cart
        Then I am shown an order subtotal
        And the subtotal does not include any delivery costs

    @NL-218
    Scenario: Displaying cart ID
      Given i navigate to "Newlook" home page
      When click on a product image or title
      And I click Add to Bag
      Then the product is added to my basket
      Then I can see a cart ID displayed

      @NL-218
      Scenario: Displaying cart ID on QUICK VIEW SIDE panel
        Given i navigate to "Newlook" home page
        When click on a product image or title
        And I click Add to Bag
        Then the product is added to my basket
        And I view my cart Quick View side panel
        Then I CANNOT see a cart ID displayed


        @NL-218
        Scenario: No products selected to add in to the cart to display cart ID
          Given i navigate to "Newlook" home page
          When I view my cart, as Quick View or full view
          Then I CANNOT see a cart ID displayed

           @NL-228
          Scenario Outline: As a Customer, I want to increase the quantities of line items in my cart, so that I can control the volume of products I am going to buy
            Given i navigate to "Newlook" home page
            When click on a product image or title
            And I click Add to Bag
            Then the product is added to my basket
            And I increase the "<quantity>" of a line item
            Then the additional products will be committed to my cart with the same "<quantity>"

            Examples:
            |quantity|
            |   2    |

            @NL-228
             Scenario: Requested quantity out of stock
               Given i navigate to "Newlook" home page
               When click on a product with limited stock
               And I click Add to Bag
               Then the product is added to my basket
               Then I am only shown the quantity available in the picker

             @guii @NL-228
             Scenario: Decrease the quantity of the item cart
               Given i navigate to "Newlook" home page
               When click on a product image or title
               Then increase the product quantity before adding to the bag
               And I click Add to Bag
               Then the product is added to my basket
               And I decrease the quantity of the line item
               Then the unwanted product/s will be removed from my cart